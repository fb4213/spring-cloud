package demo.controller.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

@WebServlet(name = "downloadServlet", urlPatterns = "/downloadServlet")
public class DownloadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Path path = Paths.get("d://tmp//hive-script-4244067715633639367.txt");
        try (FileChannel fileChannel = FileChannel.open(path)) {
            fileChannel.transferTo(0, fileChannel.size(), Channels.newChannel(resp.getOutputStream()));
        }
    }
}
