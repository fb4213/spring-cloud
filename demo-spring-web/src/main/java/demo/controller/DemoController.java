package demo.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Paths;

@RestController
public class DemoController {

    @GetMapping("/get")
    public Demo testGet(@RequestParam(defaultValue = "test") String param) {
        return new Demo(1, param);
    }

    @PostMapping("/post")
    public Demo testPost(@RequestBody Demo demo) {
        return demo;
    }

    @PostMapping("/upload")
    public void fileUpload(@RequestParam("file") MultipartFile file) {
        // 判断文件是否为空
        if (!file.isEmpty()) {
            try {
                file.transferTo(Paths.get("d://upload//" + file.getOriginalFilename()));
            } catch (IOException e) {
                // ignore
                System.out.println(e);
            }
        }
    }

    private static class Demo {
        private int id;
        private String name;

        Demo(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
