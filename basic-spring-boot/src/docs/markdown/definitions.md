
<a name="definitions"></a>
## 定义

<a name="dutytime"></a>
### DutyTime

|名称|说明|类型|
|---|---|---|
|**effectiveTime**  <br>*可选*|**样例** : `"string"`|string|
|**effectiveTimeDate**  <br>*可选*|**样例** : `"string"`|string (date-time)|
|**id**  <br>*可选*|**样例** : `0`|integer (int64)|


<a name="vodata"></a>
### VoData

|名称|说明|类型|
|---|---|---|
|**data**  <br>*可选*|**样例** : `"object"`|object|
|**msg**  <br>*可选*|**样例** : `"string"`|string|
|**success**  <br>*可选*|**样例** : `true`|boolean|



