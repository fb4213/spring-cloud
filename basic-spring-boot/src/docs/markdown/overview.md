# Spring Boot中使用Swagger2构建RESTful APIs


<a name="overview"></a>
## 概览
Spring Boot中使用Swagger2构建RESTful APIs


### 版本信息
*版本* : 1.0


### URI scheme
*域名* : localhost:8080  
*基础路径* : /


### 标签

* excel-controller : Excel Controller
* home-controller : Home Controller
* seckill-controller : Seckill Controller
* zhihu-user-controller : Zhihu User Controller



