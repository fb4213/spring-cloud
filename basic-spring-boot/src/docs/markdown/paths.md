
<a name="paths"></a>
## 资源

<a name="excel-controller_resource"></a>
### Excel-controller
Excel Controller


<a name="downloadusingget"></a>
#### download
```
GET /download
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string (byte)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `*/*`


##### HTTP请求示例

###### 请求 path
```
/download
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="excelusingget"></a>
#### excel
```
GET /excel
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string (byte)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `*/*`


##### HTTP请求示例

###### 请求 path
```
/excel
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="fileuploadusingpost"></a>
#### fileUpload
```
POST /fileUpload
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**FormData**|**file**  <br>*必填*|file|file|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `multipart/form-data`


##### 生成

* `*/*`


##### HTTP请求示例

###### 请求 path
```
/fileUpload
```


###### 请求 formData
```
json :
"file"
```


<a name="home-controller_resource"></a>
### Home-controller
Home Controller


<a name="registerusingpost"></a>
#### register
```
POST /register
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**dutyTime**  <br>*必填*|dutyTime|[DutyTime](#dutytime)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[VoData](#vodata)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `*/*`


##### HTTP请求示例

###### 请求 path
```
/register
```


###### 请求 body
```
json :
{
  "effectiveTime" : "string",
  "effectiveTimeDate" : "string",
  "id" : 0
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "string",
  "success" : true
}
```


<a name="seckill-controller_resource"></a>
### Seckill-controller
Seckill Controller


<a name="detailusingget"></a>
#### detail
```
GET /seckill/detail
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**id**  <br>*可选*|id|integer (int64)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `*/*`


##### HTTP请求示例

###### 请求 path
```
/seckill/detail
```


###### 请求 query
```
json :
{
  "id" : 0
}
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="listusingget"></a>
#### list
```
GET /seckill/list
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**currentPage**  <br>*可选*|currentPage|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `*/*`


##### HTTP请求示例

###### 请求 path
```
/seckill/list
```


###### 请求 query
```
json :
{
  "currentPage" : 0
}
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="timeusingget"></a>
#### time
```
GET /seckill/time/now
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[VoData](#vodata)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `*/*`


##### HTTP请求示例

###### 请求 path
```
/seckill/time/now
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "string",
  "success" : true
}
```


<a name="exposerusingpost"></a>
#### exposer
```
POST /seckill/{seckillId}/exposer
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**seckillId**  <br>*必填*|seckillId|integer (int64)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[VoData](#vodata)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `application/json;charset=utf-8`


##### HTTP请求示例

###### 请求 path
```
/seckill/0/exposer
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "string",
  "success" : true
}
```


<a name="executeusingpost"></a>
#### execute
```
POST /seckill/{seckillId}/{md5}/execution
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**md5**  <br>*必填*|md5|string|
|**Path**|**seckillId**  <br>*必填*|seckillId|integer (int64)|
|**Body**|**phone**  <br>*可选*|phone|integer (int64)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[VoData](#vodata)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `application/json;charset=UTF-8`


##### HTTP请求示例

###### 请求 path
```
/seckill/0/string/execution
```


###### 请求 body
```
json :
{ }
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "string",
  "success" : true
}
```


<a name="zhihu-user-controller_resource"></a>
### Zhihu-user-controller
Zhihu User Controller


<a name="datausingget"></a>
#### 知乎用户列表
```
GET /data
```


##### 说明
知乎用户统计信息


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|object|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `*/*`


##### HTTP请求示例

###### 请求 path
```
/data
```


##### HTTP响应示例

###### 响应 200
```
json :
"object"
```



