package my.springboot;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

import static my.springboot.common.Constant.NORMAL_TIME_FORMAT;

/**
 * @author fengbo
 * @date 2018/4/12
 */
//@Component
public class Schedule {

    @Scheduled(cron = "0/10 * * * * ?")
    public void schedule() {
        String dateStr = DateFormatUtils.format(System.currentTimeMillis(), NORMAL_TIME_FORMAT);
        System.out.println("每十秒执行一次,当前时间：" + dateStr);
    }
}
