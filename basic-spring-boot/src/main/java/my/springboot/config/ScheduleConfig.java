package my.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * 定时任务配置类
 * @author fengbo
 * @date 2018/5/3
 */
@Configuration
@EnableScheduling
public class ScheduleConfig {

    @Bean
    public TaskScheduler taskScheduler() {
        ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(1, r -> new Thread(r, "Scheduler"));
        return new ConcurrentTaskScheduler(executor);
    }
}
