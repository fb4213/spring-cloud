package my.springboot.config;

import com.github.wxpay.sdk.WXPayConfig;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Value;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

//@Service
public class IWxPayConfig implements WXPayConfig { // 继承sdk WXPayConfig 实现sdk中部分抽象方法

    private byte[] certData;

    @Value("${vendor.wx.config.app_id}")
    private String appId;

    @Value("${vendor.wx.pay.key}")
    private String wxPayKey;

    @Value("${vendor.wx.pay.mch_id}")
    private String wxPayMchId;

    public IWxPayConfig() throws Exception {
        try (InputStream certStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("wechat/apiclient_cert.p12")) {
            this.certData = IOUtils.toByteArray(certStream);
        }
    }

    @Override
    public String getAppID() {
        return appId;
    }

    @Override
    public String getMchID() {
        return wxPayMchId;
    }

    @Override
    public String getKey() {
        return wxPayKey;
    }

    @Override
    public InputStream getCertStream() {
        return new ByteArrayInputStream(this.certData);
    }

    @Override
    public int getHttpConnectTimeoutMs() {
        return 0;
    }

    @Override
    public int getHttpReadTimeoutMs() {
        return 0;
    }
}
