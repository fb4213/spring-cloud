package my.springboot.config;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author fengbo
 */
//@Configuration
public class RocketMQConfig {

    @Autowired
    private TransactionListener transactionListener;

    @Bean
    public TransactionMQProducer getProducer() throws MQClientException {
        TransactionMQProducer producer = new TransactionMQProducer("please_rename_unique_group_name");
        producer.setTransactionListener(transactionListener);
        // 不开启vip通道 开通口端口会减2
        producer.setVipChannelEnabled(false);
        // 绑定name server
        producer.setNamesrvAddr("118.31.167.239:9876");
        producer.start();
        return producer;
    }
}
