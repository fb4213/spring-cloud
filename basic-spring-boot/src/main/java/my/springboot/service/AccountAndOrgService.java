package my.springboot.service;

/**
 * @author fengbo
 * @date 2018/10/23
 */
public interface AccountAndOrgService {

    /**
     * HttpRequest请求
     * @param url 请求地址
     * @param requestContext 请求体
     */
    void request(String url, String requestContext);
}
