package my.springboot.service;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

public interface WechatService {
    JSONObject getWechatSession(String code);
    boolean sendMessage(String openId, String templateId, String page, String emphasisKeyword, Map<String, String> data);
    String getWechatAccessToken();
}
