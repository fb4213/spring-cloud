package my.springboot.service;

import my.springboot.domain.primary.User;
import my.springboot.domain.primary.Withdrawal;

/**
 * 消息推送
 * @author fengbo
 */
public interface MessagePushService {

    /**
     * 提现消息发送
     * @param user 用户
     * @param withdrawal 提现
     */
    void sendWithdrawalMsg(User user, Withdrawal withdrawal);

    /**
     * 微信提现成功通知发送
     * @param user 用户
     * @param withdrawal 提现
     */
    void sendWxAccountMsg(User user, Withdrawal withdrawal);

    /**
     * 测试异步任务
     * @param name
     */
    void testAsync(String name);

}
