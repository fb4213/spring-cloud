package my.springboot.service;

/**
 * 微信支付
 * @author fengbo
 */
public interface WechatPayService {

    /**
     * 微信支付到零钱
     * @param amount 支付金额（单位：分）
     * @param tradeNo 订单号
     * @param openId 被支付的用户的openId
     * @param ip 调用者IP地址
     * @return ret
     * @throws Exception ex
     */
    String payToChange(Integer amount, String tradeNo, String openId, String ip) throws Exception;
}
