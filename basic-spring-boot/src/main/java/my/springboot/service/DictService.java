package my.springboot.service;

import my.springboot.domain.primary.Dict;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author fengbo
 * @since 2019-09-17
 */
public interface DictService extends IService<Dict> {

    /**
     * 根据paramKey查询字典列表
     * @param paramKey key
     * @return ret
     */
    List<Dict> getDictList(String paramKey);
}
