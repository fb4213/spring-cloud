package my.springboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import my.springboot.domain.primary.ZhihuUser;
import my.springboot.mapper.ZhihuUserMapper;
import my.springboot.service.ZhihuUserService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 知乎用户Service
 * @author fengbo
 * @date 2017/9/4
 */
@Service
public class ZhihuUserServiceImpl extends ServiceImpl<ZhihuUserMapper, ZhihuUser> implements ZhihuUserService {

    @Override
    public long countBySex() {
        return count(new QueryWrapper<ZhihuUser>().isNull("sex"));
    }

    @Override
    public long countBySex(String sex) {
        return count(new QueryWrapper<ZhihuUser>().eq("sex", sex));
    }

    @Override
    @Cacheable("zhihu")
    public Map<String, Object> getAllPage() {
        Map<String, Object> result = new HashMap<>(6);
        getData(result, "followers");
        getData(result, "agrees");
        getData(result, "answers");
        return result;
    }

    private void getData(Map<String, Object> map, String type) {
        IPage<ZhihuUser> page = new Page<>(1, 10);
        List<ZhihuUser> userList = page(page).getRecords();
        int size = userList.size();
        int[] num = new int[size];
        String[] names = new String[size];
        for (int i = 0; i < size; i++) {
            ZhihuUser user = userList.get(i);
            names[i] = user.getUsername();
            num[i] = choose(type, user);
        }
        map.put(type, num);
        map.put(type + "Names", names);
    }

    private int choose(String type, ZhihuUser user) {
        int num = 0;
        switch (type) {
            case "followers":
                num = user.getFollowers();
                break;
            case "agrees":
                num = user.getAgrees();
                break;
            case "answers":
                num = user.getAnswers();
                break;
                default:
                    break;
        }
        return num;
    }

    private void getFollowers(Map<String, Object> map) {
        IPage<ZhihuUser> page = new Page<>(1, 10);
        List<ZhihuUser> userList = page(page, new QueryWrapper<ZhihuUser>().orderByDesc("followers")).getRecords();
        int size = userList.size();
        int[] followers = new int[size];
        String[] names = new String[size];
        for (int i = 0; i < size; i++) {
            ZhihuUser user = userList.get(i);
            followers[i] = user.getFollowers();
            names[i] = user.getUsername();
        }
        map.put("followers", followers);
        map.put("followerNames", names);
    }

    private void getAgrees(Map<String, Object> map) {
        IPage<ZhihuUser> page = new Page<>(1, 10);
        List<ZhihuUser> userList = page(page, new QueryWrapper<ZhihuUser>().orderByDesc("agrees")).getRecords();
        int size = userList.size();
        int[] agrees = new int[size];
        String[] names = new String[size];
        for (int i = 0; i < size; i++) {
            ZhihuUser user = userList.get(i);
            agrees[i] = user.getAgrees();
            names[i] = user.getUsername();
        }
        map.put("agrees", agrees);
        map.put("agreeNames", names);
    }

    private void getAnswers(Map<String, Object> map) {
        IPage<ZhihuUser> page = new Page<>(1, 10);
        List<ZhihuUser> userList = page(page, new QueryWrapper<ZhihuUser>().orderByDesc("answers")).getRecords();
        int size = userList.size();
        int[] answers = new int[size];
        String[] names = new String[size];
        for (int i = 0; i < size; i++) {
            ZhihuUser user = userList.get(i);
            answers[i] = user.getAnswers();
            names[i] = user.getUsername();
        }
        map.put("answers", answers);
        map.put("answerNames", names);
    }
}
