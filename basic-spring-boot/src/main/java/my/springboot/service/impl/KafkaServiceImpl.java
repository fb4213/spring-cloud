package my.springboot.service.impl;

import com.alibaba.fastjson.JSON;
import my.springboot.po.bo.PageBO;
import my.springboot.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * @author fengbo
 */
@Service
public class KafkaServiceImpl implements KafkaService {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void sendMsg(PageBO bo) {
        kafkaTemplate.send("kafka-test", JSON.toJSONString(bo));
    }

}
