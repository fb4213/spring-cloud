package my.springboot.service.impl;

import my.springboot.domain.primary.Dict;
import my.springboot.domain.primary.MqTransaction;
import my.springboot.mapper.MqTransactionMapper;
import my.springboot.service.DictService;
import my.springboot.service.MqTransactionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2019-12-15
 */
//@Service
public class MqTransactionServiceImpl extends ServiceImpl<MqTransactionMapper, MqTransaction> implements MqTransactionService {

    @Autowired
    private DictService dictService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveMsg(Dict dict, String msgKey) {
        dictService.save(dict);
        MqTransaction transaction = new MqTransaction();
        transaction.setMsgKey(msgKey);
        save(transaction);
    }

}
