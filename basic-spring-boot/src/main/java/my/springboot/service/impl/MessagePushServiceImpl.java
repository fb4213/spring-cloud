package my.springboot.service.impl;

import my.springboot.domain.primary.User;
import my.springboot.domain.primary.Withdrawal;
import my.springboot.po.enums.TemplateIdEnum;
import my.springboot.service.MessagePushService;
import my.springboot.service.WechatService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static my.springboot.common.Constant.NORMAL_TIME_FORMAT;

/**
 * 消息推送
 * @author fengbo
 */
@Service
public class MessagePushServiceImpl implements MessagePushService {

    @Autowired
    private WechatService wechatService;

    @Override
    public void sendWithdrawalMsg(User user, Withdrawal withdrawal) {
        // 提现消息发送
        String nowStr = DateFormatUtils.format(System.currentTimeMillis(), NORMAL_TIME_FORMAT);
        Map<String, String> data = new HashMap<>(16);
        data.put("keyword1", user.getWxNickName());
        data.put("keyword2", withdrawal.getWithdrawalCode());
        data.put("keyword3", "余额提现");
        data.put("keyword4", nowStr);
        data.put("keyword5", "微信零钱");
        data.put("keyword6", "待审核");
        data.put("keyword7", withdrawal.getAccountBalance().toString() + "元");
        data.put("keyword8", withdrawal.getRealAmount().toString() + "元");
        data.put("keyword9", nowStr);
        wechatService.sendMessage(user.getWxOpenId(), TemplateIdEnum.WITHDROW_APPLY.getId(), "", "", data);
    }

    /**
     * 异步发送消息，微信消息发送成功与否不影响提现的具体流程
     * @param user 用户
     * @param withdrawal 提现
     */
    @Async
    @Override
    public void sendWxAccountMsg(User user, Withdrawal withdrawal) {
        if (withdrawal.getAccountTime() == null) {
            withdrawal.setAccountTime(new Date());
        }
        String accountTimeStr = DateFormatUtils.format(withdrawal.getAccountTime(), NORMAL_TIME_FORMAT);
        if (withdrawal.getCreateTime() == null) {
            withdrawal.setCreateTime(new Date());
        }
        String createTimeStr = DateFormatUtils.format(withdrawal.getCreateTime(), NORMAL_TIME_FORMAT);
        String realMoney = withdrawal.getRealAmount().toString();
        Map<String, String> data = new HashMap<>(16);
        data.put("keyword1", realMoney);
        data.put("keyword2", accountTimeStr);
        data.put("keyword3", "0.00");
        data.put("keyword4", realMoney);
        data.put("keyword5", "微信零钱");
        data.put("keyword6", "打款成功");
        data.put("keyword7", withdrawal.getWithdrawalCode());
        data.put("keyword8", createTimeStr);
        data.put("keyword9", "请到微信-零钱查看提现金额");
        wechatService.sendMessage(user.getWxOpenId(), TemplateIdEnum.WITHDROW_APPLY.getId(), "", "", data);
    }

    @Async
    @Override
    public void testAsync(String name) {
        System.out.println("async start time is " + DateFormatUtils.format(System.currentTimeMillis(), NORMAL_TIME_FORMAT));
        try {
            Thread.sleep(1000 * 10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("async end ::time is " + DateFormatUtils.format(System.currentTimeMillis(), NORMAL_TIME_FORMAT) +" name is " + name);
    }
}
