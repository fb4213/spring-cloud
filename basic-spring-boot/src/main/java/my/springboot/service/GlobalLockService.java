package my.springboot.service;

import java.util.concurrent.TimeUnit;

/**
 * 未获取锁直接失败，且不可重入。
 * 实现思路：https://www.cnblogs.com/linjiqin/p/8003838.html
 * 当Redis为单点时可使用，Redis集群可以考虑使用Redisson。
 * 官方文档：http://ifeve.com/redis-lock/
 * @author fengbo
 */
public interface GlobalLockService {

    /**
     * 直接获取锁，如果失败马上返回
     * @param key key
     * @param value value
     * @return ret
     */
    boolean lock(String key, String value);

    /**
     * 指定业务锁有效期
     * @param key key
     * @param value value
     * @param expire 过期时间
     * @param expireUnit 过期时间的单位
     * @return ret
     */
    boolean lock(String key, String value, long expire, TimeUnit expireUnit);

    /**
     * 释放锁
     * @param key key
     * @param value value
     * @return ret
     */
    boolean unlock(String key, String value);
}
