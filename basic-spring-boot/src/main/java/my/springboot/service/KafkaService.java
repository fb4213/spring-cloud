package my.springboot.service;

import my.springboot.po.bo.PageBO;

/**
 * @author fengbo
 */
public interface KafkaService {

    void sendMsg(PageBO bo);
}
