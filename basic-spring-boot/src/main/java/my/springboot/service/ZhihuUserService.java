package my.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import my.springboot.domain.primary.ZhihuUser;

import java.util.Map;

/**
 * 知乎用户Service接口
 * @author fengbo
 * @date 2017/9/4
 */
public interface ZhihuUserService extends IService<ZhihuUser> {

    /**
     * 根据性别统计人数
     * @return
     */
    long countBySex();

    /**
     * 根据性别统计人数
     * @param sex
     * @return
     */
    long countBySex(String sex);

    /**
     * 获取所有用户
     * @return
     */
    Map<String, Object> getAllPage();
}
