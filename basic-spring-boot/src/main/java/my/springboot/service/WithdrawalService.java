package my.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import my.springboot.domain.primary.Withdrawal;
import my.springboot.po.bo.WithdrawalAdminBO;
import my.springboot.po.bo.WithdrawalBO;
import my.springboot.po.vo.WithdrawalAdminVo;
import my.springboot.po.vo.WithdrawalVo;

import java.math.BigDecimal;

/**
 * <p>
 * 提现历史 服务类
 * </p>
 *
 * @author fengbo
 * @since 2019-08-23
 */
public interface WithdrawalService extends IService<Withdrawal> {

    /**
     * 申请提现
     * @param amount 提现值
     * @param userIp 用户ip地址
     */
    void saveWithdrawal(BigDecimal amount, String userIp);

    /**
     * 提现审核
     * @param withdrawalVo vo
     */
    void updateWithdrawalById(WithdrawalVo withdrawalVo);

    WithdrawalAdminVo getWithdrawalList(WithdrawalAdminBO withdrawalForm);
}
