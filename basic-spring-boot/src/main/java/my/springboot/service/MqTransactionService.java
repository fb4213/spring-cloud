package my.springboot.service;

import my.springboot.domain.primary.Dict;
import my.springboot.domain.primary.MqTransaction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengbo
 * @since 2019-12-15
 */
public interface MqTransactionService extends IService<MqTransaction> {
    void saveMsg(Dict dict, String msgKey);
}
