package my.springboot.exception;

import lombok.Data;
import my.springboot.po.vo.ReturnCode;

@Data
public class BaseException extends RuntimeException {
    private int code;
    private String msg;

    public BaseException(ReturnCode returnCode) {
        super(returnCode.getMsg());
        setCode(returnCode.getCode());
        setMsg(returnCode.getMsg());
    }

    public BaseException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
