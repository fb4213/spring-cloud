package my.springboot.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Component;
import org.springframework.kafka.annotation.KafkaListener;

/**
 * @author fengbo
 */
//@Component
public class MyKafkaListener {

    @KafkaListener(id = "", topics = "kafka-test")
    public void listen(ConsumerRecord<String, String> record) {
        String value = record.value();
        System.out.println("================" + value + "=================");
    }
}
