package my.springboot;

import my.springboot.controller.DictController;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author fengbo
 * @version 1.0.0
 * @date 2018/11/22 16:20
 * @since JDK 1.8
 */
@Component
public class UserAuthInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        DictController.threadLocal.set("1111");
        System.out.println("Interceptor");

        return super.preHandle(request, response, handler);
    }
}
