package my.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.springboot.domain.primary.OcrMsg;

import java.util.List;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2019-09-17
 */
public interface OcrMsgMapper extends BaseMapper<OcrMsg> {

    List<String> queryPageIdList();

}
