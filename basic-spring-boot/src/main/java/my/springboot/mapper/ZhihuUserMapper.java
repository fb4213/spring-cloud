package my.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.springboot.domain.primary.ZhihuUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2019-09-07
 */
public interface ZhihuUserMapper extends BaseMapper<ZhihuUser> {

}
