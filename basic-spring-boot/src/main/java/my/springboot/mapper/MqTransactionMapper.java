package my.springboot.mapper;

import my.springboot.domain.primary.MqTransaction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2019-12-15
 */
public interface MqTransactionMapper extends BaseMapper<MqTransaction> {

}
