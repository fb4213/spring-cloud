package my.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.springboot.domain.primary.Dict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2019-09-17
 */
public interface DictMapper extends BaseMapper<Dict> {

}
