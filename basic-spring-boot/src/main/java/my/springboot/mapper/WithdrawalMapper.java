package my.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.springboot.domain.primary.Withdrawal;
import my.springboot.po.bo.WithdrawalAdminBO;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 提现历史 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2019-08-23
 */
public interface WithdrawalMapper extends BaseMapper<Withdrawal> {

    BigDecimal sumRealAmount(WithdrawalAdminBO withdrawalForm);

    List<Withdrawal> page();
}
