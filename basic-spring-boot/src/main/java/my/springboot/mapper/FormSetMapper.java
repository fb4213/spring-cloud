package my.springboot.mapper;

import my.springboot.domain.primary.FormSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * formId收集表 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2019-09-09
 */
public interface FormSetMapper extends BaseMapper<FormSet> {

}
