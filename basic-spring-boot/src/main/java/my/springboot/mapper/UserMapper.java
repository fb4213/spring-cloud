package my.springboot.mapper;

import my.springboot.domain.primary.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2019-09-09
 */
public interface UserMapper extends BaseMapper<User> {

}
