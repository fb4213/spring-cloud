package my.springboot;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * AOP测试
 * @author fengbo
 * @date 2018/3/21
 */
@Aspect
@Component
@Slf4j
public class LogPrint {

    @Pointcut("execution(* my.springboot.service.impl.*.*(..))")
    private void pointcutName(){}

    @Before(value = "pointcutName()")
    public void before(JoinPoint joinPoint) {
        if (log.isInfoEnabled()) {
            log.info("==== {}.{} method begin, [args] is ====", joinPoint.getThis(), joinPoint.getSignature().getName());
            for (Object obj : joinPoint.getArgs()) {
                log.info(obj.toString());
            }
            log.info("=========================================================");
        }
    }

    @AfterReturning(value = "pointcutName()")
    public void after(JoinPoint joinPoint) {
        if (log.isInfoEnabled()) {
            log.info("==== {}.{} method end ====", joinPoint.getThis(), joinPoint.getSignature().getName());
        }
    }

    // 有全局异常处理，貌似不需要这个
//    @AfterThrowing(value = "pointcutName()", throwing = "e")
//    public void afterThrowing(JoinPoint joinPoint, Throwable e) {
//        log.info("==== {}.{} throw errors ====", joinPoint.getThis(), joinPoint.getSignature().getName());
//        log.error(e.getMessage(), e);
//    }

}
