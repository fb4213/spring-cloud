package my.springboot.domain.secondary;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 秒杀成功实体类
 * @author zhangyijun
 * @date 15/10/5
 */
@Entity
@Table(name = "success_killed")
@Data
public class SuccessKilled {

    @ApiModelProperty(value = "主键", name = "seckillId", example = "1")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    private long seckillId;
    @ApiModelProperty(value = "手机号", name = "userPhone", example = "186**90")
    @NotNull
    @Size(min = 11, max = 11)
    private long userPhone;
    @ApiModelProperty(value = "商品状态", name = "state", example = "1")
    private short state;
    @ApiModelProperty(value = "创建时间", name = "createTime", example = "2010-10-20 10:10:20")
    private Date createTime;

    /** 多对一 */
    @Transient
    private Seckill seckill;

}
