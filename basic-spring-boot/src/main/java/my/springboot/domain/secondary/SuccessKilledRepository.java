package my.springboot.domain.secondary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface SuccessKilledRepository extends JpaRepository<SuccessKilled, Long> {

    /**
     * 插入秒杀成功的用户记录
     * @param seckillId
     * @param userPhone
     * @return
     */
    @Query(value = "insert ignore into success_killed(seckill_id, user_phone, state) values (?1, ?2, 0)",
            nativeQuery = true)
    @Modifying
    int insert(Long seckillId, Long userPhone);
}
