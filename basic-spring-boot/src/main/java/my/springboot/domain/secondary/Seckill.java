package my.springboot.domain.secondary;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "seckill")
@ApiModel(value = "秒杀商品实体",description = "秒杀商品实体")
@Data
public class Seckill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "主键", name = "seckillId", example = "1")
    private long seckillId;
    @ApiModelProperty(value = "商品名", name = "name", example = "手机")
    @Size(max = 100)
    private String name;
    @ApiModelProperty(value = "商品数目", name = "number", example = "10")
    private int number;
    @ApiModelProperty(value = "开始时间", name = "startTime", example = "2010-10-20 10:10:20")
    private Date startTime;
    @ApiModelProperty(value = "结束时间", name = "endTime", example = "2010-10-20 10:10:20")
    private Date endTime;
    @ApiModelProperty(value = "创建时间", name = "createTime", example = "2010-10-20 10:10:20")
    private Date createTime;

}
