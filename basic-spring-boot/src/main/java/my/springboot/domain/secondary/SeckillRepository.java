package my.springboot.domain.secondary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

/**
 * 秒杀商品查询jpa实现
 * @author fengbo
 * @date 2017/10/28
 */
public interface SeckillRepository extends JpaRepository<Seckill, Long> {

    /**
     * 减库存
     * @param seckillId
     * @param killTime
     * @return
     */
    @Query(value = "update " +
            "          seckill " +
            "        set " +
            "          number = number - 1 " +
                "        where seckill_id = ?1 " +
            "        and start_time <= ?2 " +
            "        and end_time >= ?2 " +
            "        and number > 0;", nativeQuery = true)
    @Modifying
    int reduceNumber(long seckillId, Date killTime);
}
