package my.springboot.domain.primary;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * formId收集表
 * </p>
 *
 * @author fengbo
 * @since 2019-09-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fims_form_set")
public class FormSet implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 唯一标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * openId
     */
    private String openId;

    private String formId;

    /**
     * 创建时间
     */
    private String createAt;

    /**
     * 状态：0-未使用；1-已使用
     */
    private Integer status;

    /**
     * 商品ID（点击领券和下单购买时必填）
     */
    private String goodsId;


}
