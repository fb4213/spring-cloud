package my.springboot.domain.primary;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengbo
 * @since 2019-09-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fims_user")
public class User implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 1男，0女
     */
    private Boolean gender;

    /**
     * 省
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 余额
     */
    private Integer accountMoney;

    /**
     * 正在提现的金额
     */
    private Integer withdrawalMoney;

    /**
     * 详细的地址
     */
    private String address;

    /**
     * 真实姓名
     */
    private String realname;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 最后登录时间
     */
    private Date lastTime;

    /**
     * 最后登录IP
     */
    private String lastIp;

    /**
     * 微信id
     */
    private String wxOpenId;

    /**
     * 微信unionid
     */
    private String wxUnionId;

    /**
     * 微信组
     */
    private String wxGroupId;

    /**
     * 微信头像
     */
    private String wxHeadPic;

    /**
     * 微信昵称
     */
    private String wxNickName;

    /**
     * 是否代理 0 - 否 1 是
     */
    private Integer isAgent;

    /**
     * 累积收益
     */
    private Integer income;


}
