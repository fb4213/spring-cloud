package my.springboot.domain.primary;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengbo
 * @since 2019-09-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_zhihu_user")
public class ZhihuUser implements Serializable {

private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String userToken;

    private String location;

    private String business;

    private String sex;

    private String employment;

    private String education;

    private String username;

    private String url;

    private Integer agrees;

    private Integer thanks;

    private Integer asks;

    private Integer answers;

    private Integer posts;

    private Integer followees;

    private Integer followers;

    private String position;


}
