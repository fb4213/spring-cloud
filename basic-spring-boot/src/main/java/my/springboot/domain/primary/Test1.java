package my.springboot.domain.primary;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengbo
 * @since 2019-12-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("test1")
public class Test1 implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    private String itemName;

    private String itemName1;

    private String itemName2;

    private String pageId;

    private String pageUrl;

    private String allTitle;

    private String test;


}
