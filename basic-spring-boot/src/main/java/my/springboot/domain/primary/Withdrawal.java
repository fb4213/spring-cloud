package my.springboot.domain.primary;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 提现历史
 * </p>
 *
 * @author fengbo
 * @since 2019-08-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fims_withdrawal")
public class Withdrawal implements Serializable {

private static final long serialVersionUID=1L;

    /**
     * 自增长主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 提款用户名
     */
    private String userName;

    /**
     * 提现单号
     */
    private String withdrawalCode;

    /**
     * 账户余额
     */
    private BigDecimal accountBalance;

    /**
     * 申请金额
     */
    private BigDecimal applyAmount;

    /**
     * 真实提取金额
     */
    private BigDecimal realAmount;

    /**
     * 审核状态（1-待审核，2-已审核，3-已拒绝）
     */
    private Integer auditStatus;

    /**
     * 拒绝理由
     */
    private String rejectReason;

    /**
     * 划款状态（1-未划款，2-已划款，3-划款失败）
     */
    private Integer applyStatus;

    /**
     * 到账时间
     */
    private Date accountTime;

    /**
     * 申请时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

}
