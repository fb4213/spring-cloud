package my.springboot.util;

import my.springboot.po.vo.ReturnCode;
import my.springboot.po.vo.VoData;

import java.util.Map;

/**
 * JSON 返回工具
 * @author fengbo
 */
public class ReturnUtils {

    private static final VoData SUCCESS;
    private static final VoData ERROR;

    static {
        SUCCESS = new VoData<>(ReturnCode.SUCCESS);
        ERROR = new VoData<>(ReturnCode.SYS_FAILED);
    }

    private ReturnUtils() {}

    public static VoData fail() {
        return ERROR;
    }

    public static VoData fail(ReturnCode returnCode) {
        return new VoData(returnCode);
    }

    public static VoData fail(ReturnCode returnCode, String msg) {
        return new VoData(returnCode, msg);
    }

    public static VoData<Map<String, String>> fail(ReturnCode returnCode, Map<String, String> data) {
        return new VoData<>(returnCode, data);
    }

    public static VoData success() {
        return SUCCESS;
    }

    public static VoData success(ReturnCode returnCode) {
        return new VoData(returnCode);
    }

    public static <T> VoData<T> success(T data) {
        return new VoData<>(ReturnCode.SUCCESS, data);
    }
}
