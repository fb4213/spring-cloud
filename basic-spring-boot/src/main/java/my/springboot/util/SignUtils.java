package my.springboot.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/**
 * @author fengbo
 */
public class SignUtils {

    private static String getSign(Map<String, String> data) {
        Set<String> keySet = data.keySet();
        String[] keyArr = keySet.toArray(new String[0]);
        Arrays.sort(keyArr);
        StringBuilder sb = new StringBuilder("d7046ccd9614c3fbe7134fce327edfd3");
        for (String key : keyArr) {
            if ("sign".equals(key)) {
                continue;
            }
            String value = data.get(key).trim();
            if (StringUtils.isNotBlank(value)) {
                sb.append(key).append(value);
            }
        }
        sb.append("d7046ccd9614c3fbe7134fce327edfd3");
        return DigestUtils.md5Hex(sb.toString()).toUpperCase();
    }
}
