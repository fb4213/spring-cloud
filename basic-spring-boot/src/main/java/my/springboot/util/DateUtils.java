package my.springboot.util;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 日期工具类
 * DAY_OF_WEEK_IN_MONTH 是某月中第几周
 * 和WEEK_OF_MONTH的区别是从周日开始计算，DAY_OF_WEEK_IN_MONTH是从每月起始日开始计算
 * @author fengbo
 * @date 2018/6/9
 */
public class DateUtils {

    /**
     * 获取参数的上个月的第一天的毫秒数
     * @param date 毫秒数
     * @return 参数的上个月的第一天的毫秒数
     */
    public static long getFirstDateOfLastMonth(long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getActualMinimum(Calendar.MILLISECOND));
        return calendar.getTimeInMillis();
    }

    /**
     * 获取参数的上个月的最后一天的毫秒数
     * @param date 毫秒数
     * @return 参数的上个月的最后一天的毫秒数
     */
    public static long getLastDateOfLastMonth(long date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));
        return calendar.getTimeInMillis();
    }

    /**
     * 获取参数的上个月的年份
     * @param date 毫秒数
     * @return 年份
     */
    public static int getYearOfLastMonth(long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        calendar.add(Calendar.MONTH, -1);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 获取参数的上个月的月份
     * @param date 毫秒数
     * @return 月份
     */
    public static int getMonthOfLastMonth(long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        calendar.add(Calendar.MONTH, -1);
        // Java中0是第一个月，1是星期日，7是星期六。
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 获取每月第一天零时零分零秒
     * @return date
     */
    public static Date getMonthOfFirstDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取当天0点0分0秒
     * @return date
     */
    public static Date getBeginTimeForDay() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        return cal.getTime();
    }

    /**
     * 获取当天24点0分0秒
     * @return date
     */
    public static Date getEndTimeForDay() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 24, 0, 0);
        return cal.getTime();
    }

    /**
     * 将毫秒数转换成秒数
     * @param millis 毫秒数
     * @return 秒数
     */
    public static long convertMillisToSeconds(long millis) {
        return TimeUnit.SECONDS.convert(millis, TimeUnit.MILLISECONDS);
    }
}
