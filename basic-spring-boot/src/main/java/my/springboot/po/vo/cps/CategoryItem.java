package my.springboot.po.vo.cps;

import lombok.Data;

import java.util.List;

@Data
public class CategoryItem {
    private List<String> categoryName;
}
