package my.springboot.po.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 提现列表下载Vo
 * @author fengbo
 */
@Data
public class WithdrawalAdminListVo {

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 提款用户名
     */
    private String userName;

    /**
     * 提现单号
     */
    private String withdrawalCode;

    /**
     * 账户余额
     */
    private BigDecimal accountBalance;

    /**
     * 申请金额
     */
    private BigDecimal applyAmount;

    /**
     * 真实提取金额
     */
    private BigDecimal realAmount;

    /**
     * 审核状态（1-待审核，2-已审核，3-已拒绝）
     */
    private String auditStatus;

    /**
     * 拒绝理由
     */
    private String rejectReason;

    /**
     * 划款状态（1-未划款，2-已划款，3-划款失败）
     */
    private String applyStatus;

    /**
     * 到账时间
     */
    private String accountTime;

    /**
     * 申请时间
     */
    private String createTime;
}
