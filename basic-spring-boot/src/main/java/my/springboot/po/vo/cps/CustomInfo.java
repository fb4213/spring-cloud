package my.springboot.po.vo.cps;

import lombok.Data;

import java.util.List;

@Data
public class CustomInfo {

    private List<Custom> customList;

}
