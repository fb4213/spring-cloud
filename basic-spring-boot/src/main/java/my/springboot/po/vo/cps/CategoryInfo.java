package my.springboot.po.vo.cps;

import lombok.Data;

@Data
public class CategoryInfo {
    private CategoryItem categoryItem;
}
