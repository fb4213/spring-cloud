package my.springboot.po.vo.cps;

import lombok.Data;

@Data
public class SkuItem {
    private String skuId;
    private String barcodeType;
    private Long barcode;
    private ImageInfo imageInfo;
    private String linkUrl;
    private PriceInfo priceInfo;
    private SaleInfo saleInfo;
    private ShopInfo shopInfo;
}
