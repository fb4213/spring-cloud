package my.springboot.po.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * 知乎用户资料
 * @author fengbo
 */
@Entity
@Table(name = "t_zhihu_user")
@Data
public class ZhihuUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "id", name = "id", example = "1")
    private Long id;
    @ApiModelProperty(value = "用户名", name = "username", example = "username")
    private String username;
    @ApiModelProperty(value = "user token", name = "userToken", example = "user token")
    private String userToken;
    @ApiModelProperty(value = "位置", name = "location", example = "河南...")
    private String location;
    @ApiModelProperty(value = "行业", name = "business", example = "计算机")
    private String business;
    @ApiModelProperty(value = "性别", name = "sex", example = "男")
    private String sex;
    @ApiModelProperty(value = "企业", name = "employment", example = "阿里")
    private String employment;
    @ApiModelProperty(value = "企业职位", name = "position", example = "程序员")
    private String position;
    @ApiModelProperty(value = "教育", name = "education", example = "清华")
    private String education;
    @ApiModelProperty(value = "用户首页url", name = "url", example = "http://...")
    private String url;
    @ApiModelProperty(value = "答案赞同数", name = "agrees", example = "100")
    private Integer agrees;
    @ApiModelProperty(value = "感谢数", name = "thanks", example = "100")
    private Integer thanks;
    @ApiModelProperty(value = "提问数", name = "asks", example = "100")
    private Integer asks;
    @ApiModelProperty(value = "回答数", name = "answers", example = "100")
    private Integer answers;
    @ApiModelProperty(value = "文章数", name = "posts", example = "100")
    private Integer posts;
    @ApiModelProperty(value = "关注人数", name = "followees", example = "100")
    private Integer followees;
    @ApiModelProperty(value = "粉丝数量", name = "followers", example = "100")
    private Integer followers;

}
