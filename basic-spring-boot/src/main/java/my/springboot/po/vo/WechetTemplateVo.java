package my.springboot.po.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class WechetTemplateVo implements Serializable {
    /**
     * 模板id
     */
    private String templateId;
    /**
     * 接受者openId
     */
    private String touser;
    /**
     * 点击模板卡片后的跳转页面
     */
    private String page;
    /**
     * 表单提交场景下
     */
    private String formId;
    /**
     * 模板需要放大的关键词
     */
    private String emphasisKeyword;
    /**
     * 模板内容
     */
    private Map<String, String> data;
}
