package my.springboot.po.vo;

import lombok.Data;
import my.springboot.po.enums.WithdrawalApplyEnum;
import my.springboot.po.enums.WithdrawalAuditEnum;

import java.math.BigDecimal;

/**
 * 提现前台列表
 * @author fengbo
 */
@Data
public class WithdrawalListVo {
    private Long id;
    private BigDecimal realAmount;
    private Integer status;
    private String createTime;

    public void setStatus(int auditStatus, int applyStatus) {
        if (WithdrawalAuditEnum.UN_REVIEWED.getCode() == auditStatus
                && WithdrawalApplyEnum.UN_REVIEWED.getCode() == applyStatus) {
            // 审核中
            status = 0;
        } else if (WithdrawalAuditEnum.APPROVED.getCode() == auditStatus
                && WithdrawalApplyEnum.APPROVED.getCode() == applyStatus) {
            // 已到账
            status = 1;
        } else if ((WithdrawalAuditEnum.REJECTED.getCode() == auditStatus
                && WithdrawalApplyEnum.UN_REVIEWED.getCode() == applyStatus)
                || (WithdrawalAuditEnum.APPROVED.getCode() == auditStatus
                && WithdrawalApplyEnum.FAILED.getCode() == applyStatus)) {
            // 打款失败
            status = 2;
        }
    }
}
