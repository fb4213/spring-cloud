package my.springboot.po.vo.cps;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PriceInfo {

    private BigDecimal minPrice;
    private BigDecimal maxPrice;
    private BigDecimal minOriPrice;
    private BigDecimal maxOriPrice;
}
