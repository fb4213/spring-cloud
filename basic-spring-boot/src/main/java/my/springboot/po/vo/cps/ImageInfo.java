package my.springboot.po.vo.cps;

import lombok.Data;

import java.util.List;

@Data
public class ImageInfo {

    private List<MainImage> mainImageList;

}
