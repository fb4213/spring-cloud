package my.springboot.po.vo.cps;

import lombok.Data;

@Data
public class Custom {
    private String key;
    private String value;
}
