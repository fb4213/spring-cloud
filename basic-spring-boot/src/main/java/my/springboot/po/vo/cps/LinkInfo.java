package my.springboot.po.vo.cps;

import lombok.Data;

@Data
public class LinkInfo {

    private String url;
    private String wxaAppid;
    private String linkType;
}
