package my.springboot.po.vo;

import lombok.Data;

/**
 * @author fengbo
 */
@Data
public class WithdrawalVo {
    /** 主键 */
    private Long id;
    /** 状态 true-同意 false-拒绝 */
    private Boolean status;
    /** 拒绝理由 */
    private String value;
    /** IP地址 */
    private String ipAddress;
}
