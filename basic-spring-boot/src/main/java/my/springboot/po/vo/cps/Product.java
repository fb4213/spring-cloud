package my.springboot.po.vo.cps;

import lombok.Data;

@Data
public class Product {

    private String pid;
    private ImageInfo imageInfo;
    private CategoryInfo categoryInfo;
    private OfficialCategoryInfo officialCategoryInfo;
    private LinkInfo linkInfo;
    private String title;
    private String subTitle;
    private String brand;
    private ShopInfo shopInfo;
    private String desc;
    private PriceInfo priceInfo;
    private SaleInfo saleInfo;
    private CustomInfo customInfo;
    private SkuInfo skuInfo;
    private Integer partialUpdate;
}
