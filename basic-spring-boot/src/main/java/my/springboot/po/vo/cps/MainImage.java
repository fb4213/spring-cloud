package my.springboot.po.vo.cps;

import lombok.Data;

@Data
public class MainImage {
    private String url;
}
