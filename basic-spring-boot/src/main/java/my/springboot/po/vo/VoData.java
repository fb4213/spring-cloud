package my.springboot.po.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 前端公用实体类
 * @author fengbo
 * @date 2017/10/26
 */
public class VoData<T> {

    @ApiModelProperty(value = "状态码: 0 表示成功; 非 0 表示失败", example = "0", required = true)
    private int code;

    @ApiModelProperty(value = "提示信息", example = "成功", required = true)
    private String message;

    @ApiModelProperty(value = "数据", required = true)
    private T data;

    public VoData() {}

    public VoData(ReturnCode returnCode) {
        this.code = returnCode.getCode();
        this.message = returnCode.getMsg();
    }

    public VoData(ReturnCode returnCode, T data) {
        this(returnCode.getCode(), returnCode.getMsg(), data);
    }

    public VoData(ReturnCode returnCode, String message) {
        this(returnCode.getCode(), message, null);
    }

    public VoData(int code, String message, T data) {
        this.code = code;
        this.message = message;
        if (data != null) {
            this.data = data;
        }
    }

    public int getCode() {
        return code;
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
