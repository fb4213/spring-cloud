package my.springboot.po.vo.cps;

import lombok.Data;

@Data
public class SaleInfo {
    private String saleStatus;
    private Integer stock;
}
