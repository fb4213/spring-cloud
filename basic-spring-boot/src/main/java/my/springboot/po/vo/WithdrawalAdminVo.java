package my.springboot.po.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author fengbo
 */
@Data
public class WithdrawalAdminVo {

    private List<WithdrawalAdminListVo> list;

    private BigDecimal totalRealAmount;
    private BigDecimal totalApplyAmount;
}
