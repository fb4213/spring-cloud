package my.springboot.po.vo.cps;

import lombok.Data;

@Data
public class OfficialCategoryInfo {
    private CategoryItem categoryItem;
}
