package my.springboot.po.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 提现划款状态枚举
 * @author fengbo
 */
public enum  WithdrawalApplyEnum {
    /** 未划款 */
    UN_REVIEWED(0, "未划款"),
    /** 已划款 */
    APPROVED(1, "已划款"),
    /** 划款失败 */
    FAILED(2, "划款失败"),
    ;

    private int code;

    private String msg;

    WithdrawalApplyEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public static String getMsgByCode(int code) {
        for (WithdrawalApplyEnum withdrawalApplyEnum : WithdrawalApplyEnum.values()) {
            if (withdrawalApplyEnum.getCode() == code) {
                return withdrawalApplyEnum.msg;
            }
        }
        return StringUtils.EMPTY;
    }
}
