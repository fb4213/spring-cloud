package my.springboot.po.enums;

/**
 * 微信消息发送模板
 * @author fengbo
 */
public enum TemplateIdEnum {
    /**
     * 用户昵称:keyword1
     * 提现单号:keyword2
     * 提现方式:keyword3
     * 提现申请时间:keyword4
     * 提现到:keyword5
     * 提现状态:keyword6
     * 账户余额:keyword7
     * 提现金额:keyword8
     * 提现时间:keyword9
     * */
    WITHDROW_APPLY("Gk06uHhiNzwQcw48Gwl3NBXO9jhYJKFhSj2gPqx1ZnI", "提现申请通知"),
    /**
     * 到账金额:keyword1
     * 到账时间:keyword2
     * 手续费:keyword3
     * 提现金额:keyword4
     * 提现至:keyword5
     * 提现状态:keyword6
     * 订单编号:keyword7
     * 提现申请时间:keyword8.
     * 温馨提示:keyword9
     * */
    WITHDROW_SUCCESS("H7M_Rvq0A-hLx4laZ5YivsfNvodWKbgyL_EPIci18Hk", "提现到账通知"),
    /**
     * 商品名称:keyword1
     * 下单时间:keyword2
     * 订单金额:keyword3
     * 状态:keyword4
     * 订单状态:keyword5
     * 订单编号:keyword6
     * */
    ORDER_SUCCESS("UClmIHK0wAk-s-_qktHlZi_cIHRxgHMBEfzT9dbNieo", "订单提交成功通知"),
    /**
     * 商品名称:keyword1
     * 订单号:keyword2
     * 订单状态:keyword3
     * 订单金额:keyword4
     * 订单时间:keyword5
     * 温馨提示:keyword6
     * */
    ORDER_CANCEL("apnfs4WAsosxz7sPOi9bz6bnF4e6-2wcknK4sTiHImw", "订单取消通知");

    TemplateIdEnum(String id, String msg) {
        this.id = id;
        this.msg = msg;
    }

    private String id;
    private String msg;

    public String getId() {
        return id;
    }

    public String getMsg() {
        return msg;
    }
}
