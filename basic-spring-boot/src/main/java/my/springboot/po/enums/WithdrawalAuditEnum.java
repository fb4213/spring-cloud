package my.springboot.po.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 提现审核状态枚举
 * @author fengbo
 */
public enum WithdrawalAuditEnum {
    /** 待审核 */
    UN_REVIEWED(0, "待审核"),
    /** 已审核 */
    APPROVED(1, "已审核"),
    /** 已拒绝 */
    REJECTED(2, "已拒绝"),
    ;

    private int code;
    private String msg;

    WithdrawalAuditEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public static String getMsgByCode(int code) {
        for (WithdrawalAuditEnum withdrawalAuditEnum : WithdrawalAuditEnum.values()) {
            if (withdrawalAuditEnum.getCode() == code) {
                return withdrawalAuditEnum.msg;
            }
        }
        return StringUtils.EMPTY;
    }
}
