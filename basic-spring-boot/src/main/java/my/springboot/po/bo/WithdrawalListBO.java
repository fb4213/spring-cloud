package my.springboot.po.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author fengbo
 */
@Data
public class WithdrawalListBO {

    private Integer year;
    private Integer month;
    private Integer status;
}
