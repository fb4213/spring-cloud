package my.springboot.po.bo;

import lombok.Data;

/**
 * @author fengbo
 */
@Data
public class WithdrawalBO {
    private Long id;
    private Boolean status;
    private String value;
}
