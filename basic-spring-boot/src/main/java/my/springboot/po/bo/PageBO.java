package my.springboot.po.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 分页查询参数
 * @author fengbo
 */
@Data
public class PageBO {

    @ApiModelProperty(value = "页大小", required = true)
    @NotNull(message = "页大小不能为空")
    private Integer pageSize;

    @ApiModelProperty(value = "页码", required = true)
    @NotNull(message = "页码不能为空")
    private Integer pageNum;

    public PageBO() {}

    public PageBO(@NotNull(message = "页大小不能为空") Integer pageSize, @NotNull(message = "页码不能为空") Integer pageNum) {
        this.pageSize = pageSize;
        this.pageNum = pageNum;
    }
}
