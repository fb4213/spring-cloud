package my.springboot.po.bo;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author fengbo
 */
@Data
public class WithdrawalAdminBO {

    /**
     * 提款用户名
     */
    private String userName;

    /**
     * 提现单号
     */
    private String withdrawalCode;

    /**
     * 提取金额（按照真实提取金额查询）
     */
    private String beginAmount;

    public BigDecimal getBeginAmountDecimal() {
        if (StringUtils.isNotBlank(beginAmount)) {
            return new BigDecimal(beginAmount);
        }
        return null;
    }

    private String endAmount;

    public BigDecimal getEndAmountDecimal() {
        if (StringUtils.isNotEmpty(getEndAmount())) {
            return new BigDecimal(getEndAmount());
        }
        return null;
    }

    /**
     * 到账时间
     */
    private String startAccountTime;

    private String endAccountTime;

    /**
     * 申请时间
     */
    private String startCreateTime;

    private String endCreateTime;

    /**
     * 审核状态
     */
    private Integer auditStatus;

    /**
     * 划款状态
     */
    private Integer applyStatus;


}
