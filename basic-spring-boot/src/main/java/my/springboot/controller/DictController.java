package my.springboot.controller;


import my.springboot.domain.primary.Dict;
import my.springboot.po.vo.VoData;
import my.springboot.service.DictService;
import my.springboot.util.ReturnUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2019-09-17
 */
@RestController
@RequestMapping("/dict")
public class DictController {

    public static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    @Autowired
    private DictService dictService;

    @GetMapping("/list")
    public VoData<List<Dict>> getDictList(String paramKey) {
        return ReturnUtils.success(dictService.getDictList(paramKey));
    }

    @PostMapping("/test")
    public VoData<String> test(@RequestParam("file") MultipartFile file) throws IOException {
        System.out.println(Thread.currentThread().getName());
        System.out.println("test threadLocal=" + threadLocal.get());
        return ReturnUtils.success("sss");
    }

    @GetMapping("test1")
    public VoData<String> test1() throws IOException {
        System.out.println(Thread.currentThread().getName());
        System.out.println("test1 threadLocal1=" + threadLocal.get());
        threadLocal.set("2222");
        System.out.println("test1 threadLocal2=" + threadLocal.get());
        return ReturnUtils.success("sss");
    }
}

