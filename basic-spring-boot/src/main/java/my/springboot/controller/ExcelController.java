package my.springboot.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import my.springboot.domain.secondary.Seckill;
import my.springboot.util.ExcelUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 测试中会用到web项目
 * @author fengbo
 * @date 2018/2/5
 */
@Api(value = "/", tags = "Excel处理模块 by fengbo")
@Controller
public class ExcelController {

    private static final Logger log = LoggerFactory.getLogger(ExcelController.class);

    @ApiOperation("Excel导出")
    @GetMapping(value = "/excel")
    public ResponseEntity<byte[]> excel() throws Exception {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream is = getClass().getClassLoader()
                .getResourceAsStream("templates/demoTemplate.xlsx");

        Seckill demo = new Seckill();
        demo.setSeckillId(1L);
        demo.setName("test");
        demo.setNumber(1);
        demo.setCreateTime(new Date());
        demo.setEndTime(new Date());
        demo.setStartTime(new Date());
        List<Seckill> list = new ArrayList<>(1);
        list.add(demo);

        ExcelUtils.export(is, list, out);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "demoTemplate.xlsx");
        return new ResponseEntity<>(out.toByteArray(), headers, HttpStatus.CREATED);
    }

    @ApiOperation("文件下载")
    @RequestMapping(value = "download", method = RequestMethod.GET)
    public ResponseEntity<byte[]> download() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "demoTemplate.xlsx");
        return new ResponseEntity<>(FileCopyUtils.copyToByteArray(ResourceUtils.getFile(
                "classpath:templates/demoTemplate.xlsx")), headers, HttpStatus.CREATED);
    }

    @ApiOperation("文件上传")
    @RequestMapping(value = "fileUpload", method = RequestMethod.POST)
    public void fileUpload(@RequestParam("file") MultipartFile file) {
        // 判断文件是否为空
        if (!file.isEmpty()) {
            // 文件保存路径
            String filePath = getClass().getClassLoader().getResource("").getPath()
                    + "upload/" + file.getOriginalFilename();
            // 转存文件
            try {
                file.transferTo(new File(filePath));
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

}
