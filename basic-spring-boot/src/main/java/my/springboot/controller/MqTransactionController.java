package my.springboot.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2019-12-15
 */
@RestController
@RequestMapping("/mq-transaction")
public class MqTransactionController {

}

