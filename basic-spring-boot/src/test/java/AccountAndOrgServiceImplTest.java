import my.springboot.Application;
import my.springboot.service.AccountAndOrgService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class AccountAndOrgServiceImplTest {

    @Autowired
    private AccountAndOrgService accountAndOrgService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testRequest() throws IOException {

        String url = "http://localhost/test";
        String requestContext = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:ser=\"http://service.core.risen.com\">" +
                "   <soapenv:Header/>" +
                "   <soapenv:Body>" +
                "      <ser:syncOrgFrame>" +
                "         <ser:in0>2010-10-20 23:10:00</ser:in0>" +
                "      </ser:syncOrgFrame>" +
                "   </soapenv:Body>" +
                "</soapenv:Envelope>";

        accountAndOrgService.request(url, requestContext);
    }
}