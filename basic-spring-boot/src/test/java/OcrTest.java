import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONReader;
import com.baidu.aip.ocr.AipOcr;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.Data;
import my.springboot.Application;
import my.springboot.domain.primary.OcrMsg;
import my.springboot.mapper.OcrMsgMapper;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class OcrTest {

    @Autowired
    private OcrMsgMapper ocrMsgMapper;

    private ExecutorService executor = new ThreadPoolExecutor(50, 50, 1, TimeUnit.DAYS, new LinkedBlockingQueue<>(), runnable -> {
        Thread t = new Thread(runnable);
        t.setDaemon(false);
        return t;
    });

    @Test
    public void testOcr() {
        List<String> stringList = ocrMsgMapper.queryPageIdList();
        CountDownLatch countDownLatch = new CountDownLatch(stringList.size());
        for (String pageId : stringList) {
            executor.execute(() -> {
                try {
                    File file = new File("e:/Image/img3/" + pageId + ".jpg");
                    if (file.exists()) {
                        if (file.length() == 0) {
                            OcrMsg ocrMsg = new OcrMsg();
                            ocrMsg.setPageId(pageId);
                            ocrMsgMapper.insert(ocrMsg);
                            return;
                        }
                        HttpEntity httpEntity = MultipartEntityBuilder.create()
                                .addTextBody("apikey", "PKMXB9321888A")
                                .addTextBody("language", "chs")
                                .addTextBody("isOverlayRequired", "True")
                                .addBinaryBody("file ", file).build();
                        HttpPost httpPost = new HttpPost("https://apipro2.ocr.space/parse/image");
                        httpPost.setEntity(httpEntity);
                        try (CloseableHttpClient client = HttpClients.createDefault();
                             CloseableHttpResponse response = client.execute(httpPost);
                             FileOutputStream fos = new FileOutputStream("e:/Image/img3/txt/" + pageId + ".txt")) {
                            InputStream is = response.getEntity().getContent();
                            byte[] buff = new byte[2048];
                            int i;
                            while ((i = is.read(buff)) > 0) {
                                fos.write(buff, 0, i);
                            }
                            OcrMsg ocrMsg = new OcrMsg();
                            ocrMsg.setPageId(pageId);
                            ocrMsgMapper.insert(ocrMsg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        try {
            Thread.sleep(1000 * 60 * 60 * 24);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTxt() throws FileNotFoundException {
        List<OcrMsg> ocrMsgList = ocrMsgMapper.selectList(new LambdaQueryWrapper<OcrMsg>().isNull(OcrMsg::getOcrMsg));
        ocrMsgList.forEach(ocrMsg -> {
            File file = new File("e://Image//img3//txt//" + ocrMsg.getPageId() + ".txt");
            if (!file.exists()) {
                return;
            }
            try (FileReader fileReader = new FileReader(file)) {
                JSONReader reader = new JSONReader(fileReader);
                reader.startObject();
                while (reader.hasNext()) {
                    String key = reader.readString();
                    if (!"ParsedResults".equals(key)) {
                        reader.readObject();
                        continue;
                    }
                    reader.startArray();
                    reader.startObject();
                    while (reader.hasNext()) {
                        key = reader.readString();
                        if ("TextOverlay".equals(key)) {
                            StringBuilder sb = new StringBuilder();
                            reader.startObject();
                            while (reader.hasNext()) {
                                key = reader.readString();
                                if ("Lines".equals(key)) {
                                    reader.startArray();
                                    while (reader.hasNext()) {
                                        Demo demo = reader.readObject(Demo.class);
                                        sb.append(demo.getLineText()).append("\n");
                                    }
                                    reader.endArray();
                                    ocrMsg.setOcrMsg(sb.toString());
                                    ocrMsgMapper.updateById(ocrMsg);
                                    return;
                                } else {
                                    reader.readObject();
                                }
                            }
                        } else {
                            reader.readObject();
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("==============" + ocrMsg.getPageId() + "===================");
                e.printStackTrace();
            }
        });


//        String result = EntityUtils.toString(response.getEntity(), Consts.UTF_8);
//        JSONObject jsonObject = JSON.parseObject(result);
//        JSONArray parsedResults = jsonObject.getJSONArray("ParsedResults");
//        if (parsedResults.size() > 0) {
//            String parsedText = parsedResults.getJSONObject(0).getString("ParsedText");
//            OcrMsg ocrMsg = new OcrMsg();
//            ocrMsg.setOcrMsg(parsedText);
//            ocrMsg.setPageId(pageId);
//            ocrMsgMapper.insert(ocrMsg);
//        }
    }

    @Data
    private static class Demo {
        private String lineText;
        private Integer maxHeight;
        private Integer minTop;
    }



}
