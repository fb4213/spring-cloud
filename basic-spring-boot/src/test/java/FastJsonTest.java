import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.houbb.data.factory.core.util.DataUtil;
import lombok.Data;
import my.springboot.po.vo.cps.Tencent;
import org.junit.Test;

public class FastJsonTest {
    @Test
    public void testFastJson() {
        Tencent tencent = DataUtil.build(Tencent.class);
        // 生产环境中，config要做singleton处理，要不然会存在性能问题
        SerializeConfig config = new SerializeConfig();
        config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;
        String json = JSON.toJSONString(tencent, config);
        System.out.println(json);
    }

    @Test
    public void testFastJsonSort() {
        Demo demo = new Demo();
        String str = JSON.toJSONString(demo, SerializerFeature.SortField);
        System.out.println(str);
    }

    @Data
    static class Demo {
        String bbb = "b";
        String c = "c";
        String a = "a";
        String fdd = "f";
        String d = "d";
    }
}
