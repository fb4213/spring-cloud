import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import my.springboot.Application;
import my.springboot.domain.primary.Test1;
import my.springboot.mapper.Test1Mapper;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class TableTest {

    @Autowired
    private Test1Mapper test1Mapper;

    @Test
    public void testMapper() {
        List<Test1> list = test1Mapper.selectList(new LambdaQueryWrapper<Test1>().eq(Test1::getItemName, ""));
        for (Test1 test : list) {
            String test1 = test.getTest();
            if (StringUtils.isEmpty(test1)) {
                continue;
            }
            JSONObject jsonObject = JSON.parseObject(test1);
            JSONObject data = jsonObject.getJSONObject("data");
            JSONArray ke = data.getJSONArray("ke");
            if (ke.size() < 1) {
                continue;
            }
            Object obj0 = ke.get(0);
            JSONObject value0 = (JSONObject) obj0;
            test.setItemName(value0.getString("word"));

            if (ke.size() < 2) {
                continue;
            }
            Object obj1 = ke.get(1);
            JSONObject value1 = (JSONObject) obj1;
            test.setItemName1(value1.getString("word"));

            if (ke.size() < 3) {
                continue;
            }
            Object obj2 = ke.get(2);
            JSONObject value2 = (JSONObject) obj2;
            test.setItemName2(value2.getString("word"));
            test1Mapper.updateById(test);
        }
    }
}
