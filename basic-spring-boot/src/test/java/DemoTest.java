import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.Consts;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DemoTest {

    @Test
    public void testTaokouling() {
        String str = "【上海永久折叠自行车成人20寸男女式超轻便减震迷你代步变速单车】https://m.tb.cn/h.ekWNRTf 点击链接，再选择浏览器咑閞；或椱ァ製这段描述￥GoZpYk6sfwV￥后到◇綯℡寳";
        Pattern pattern = Pattern.compile("\\x{ffe5}([a-zA-Z0-9]{11})\\x{ffe5}");
        boolean b = pattern.matcher(str).find();
        System.out.println(b);
    }

    @Test
    public void testEL() {
        String txt = "https://mobile.yangkeduo.com/goods1.html?goods_id=30111527204&page_from=35&share_uin=3CCCYNAQGL3LMHAXR7MSDRXSEU_GEXDA&refer_share_id=6d07cd7ae9b043039ac43e79e273feb9&refer_share_uid=3740838110705&refer_share_channel=message";

        String re1 = ".*?((?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3}))(?:[0]?[1-9]|[1][012])(?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])";    // YYYYMMDD 1

        Pattern p = Pattern.compile(re1, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(txt);
        if (m.find()) {
            String yyyymmdd1 = m.group(1);
            System.out.print("(" + yyyymmdd1.toString() + ")" + "");
        }
    }

    @Test
    public void testPDD() {
        String shopId = "8813254569";
        String pid = "8924782_104334838";
        long now = System.currentTimeMillis() / 1000;
        String clientSecret = "4c3a378ad91d40429f4bba5c55c27e3ec54e59ce";
        String str = clientSecret + "client_id28ec1dabdd21464a965a7ba0c97e68d1data_typeJSONgenerate_we_apptruegoods_id_list[" + shopId +
                "]p_id" + pid + "timestamp" + now + "typepdd.ddk.goods.promotion.url.generate" + clientSecret;
        String sign = DigestUtils.md5Hex(str).toUpperCase();
        String url = "https://gw-api.pinduoduo.com/api/router" +
                "?client_id=28ec1dabdd21464a965a7ba0c97e68d1" +
                "&data_type=JSON" +
                "&generate_we_app=true" +
                "&goods_id_list=[" + shopId + "]" +
                "&p_id=" + pid +
                "&timestamp=" + now +
                "&type=pdd.ddk.goods.promotion.url.generate" +
                "&sign=" + sign;
        HttpGet httpGet = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpGet)) {
            if (response != null && response.getEntity() != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String result = EntityUtils.toString(response.getEntity(), Consts.UTF_8);
                System.out.println(result);
            }
        } catch (IOException e) {
        }
    }

    @Test
    public void testRedirct() throws IOException {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        HttpContext httpContext = new BasicHttpContext();
        try {
            httpClient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet("https://m.tb.cn/h.elZcTe7?sm=ff8cc1");
            httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
            response = httpClient.execute(httpGet, httpContext);
            String result = EntityUtils.toString(response.getEntity(), Consts.UTF_8);
            System.out.println(result);
//            HttpHost currentHost = (HttpHost) httpContext.getAttribute(HttpCoreContext.HTTP_TARGET_HOST);
//            HttpUriRequest req = (HttpUriRequest) httpContext.getAttribute(HttpCoreContext.HTTP_REQUEST);
//            System.out.println(req.getURI().isAbsolute() ? req.getURI().toString() : currentHost.toURI() + req.getURI());
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
            if (response != null) {
                response.close();
            }
        }
    }

    @Test
    public void testTime() throws UnsupportedEncodingException {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.DAY_OF_MONTH, 1);
//        calendar.set(Calendar.HOUR_OF_DAY, 0);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND,0);
//        calendar.set(Calendar.MILLISECOND, 0);
//        Date time = calendar.getTime();
//        System.out.println(time);
//        System.out.println(
//                Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
//        String content = "城市化";
//        content = URLEncoder.encode(content.trim(), "UTF-8");
//        System.out.println(content);
//
//        Calendar cal = Calendar.getInstance();
//        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 24, 0, 0);
//        Date time = cal.getTime();

//        System.out.println();
//
//        long millis = System.currentTimeMillis();
//        long convert = TimeUnit.SECONDS.convert(millis, TimeUnit.MILLISECONDS);
//        System.out.println(millis);
//        System.out.println(convert);

        LocalDate date = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("当前日期=" + localDateTime);

    }

    @Test
    public void testQueue() throws InterruptedException {
        BlockingQueue<String> queue = new ArrayBlockingQueue<>(3);
        queue.put("aaa");
        queue.put("aaa");
        queue.put("aaa");
        queue.put("aaa");
        queue.put("aaa");
        queue.put("aaa");
    }
}
