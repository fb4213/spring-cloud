package my.springboot.starter;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ConditionalOnBean         //	当给定的在bean存在时,则实例化当前Bean
 * ConditionalOnMissingBean  //	当给定的在bean不存在时,则实例化当前Bean
 * ConditionalOnClass        //	当给定的类名在类路径上存在，则实例化当前Bean
 * ConditionalOnMissingClass //	当给定的类名在类路径上不存在，则实例化当前Bean
 * EnableDemoConfiguration这个Bean存在时才会初始化
 * @author fengbo
 */
@Configuration
@ConditionalOnBean(annotation = EnableDemoConfiguration.class)
@EnableConfigurationProperties(DemoProperties.class)
public class DemoAutoConfiguration {

    /**
     * ConditionalOnMissingBean表示
     * DemoService对象不存在时初始化DemoService
     */
    @Bean
    @ConditionalOnMissingBean
    DemoService demoService (){
        return new DemoService();
    }
}
