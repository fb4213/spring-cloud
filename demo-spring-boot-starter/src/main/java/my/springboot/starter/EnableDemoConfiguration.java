package my.springboot.starter;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author fengbo
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(DemoRegisterar.class)
public @interface EnableDemoConfiguration {
}
