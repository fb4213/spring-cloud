package my.chat.config.interceptor;

import my.chat.common.enums.VoEnum;
import my.chat.common.execption.NotAuthException;
import my.chat.util.JwtUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author fengbo
 */
@Component
public class UserAuthInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (HttpMethod.OPTIONS.name().equals(request.getMethod())) {
            return true;
        }
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.isEmpty(token)) {
            throw new NotAuthException(VoEnum.NOT_AUTH);
        }
        String username = JwtUtils.verifyTokenAndGetUsername(token);
        if (StringUtils.isEmpty(username)) {
            throw new NotAuthException(VoEnum.NOT_AUTH);
        }
        request.setAttribute("username", username);
        return super.preHandle(request, response, handler);
    }
}
