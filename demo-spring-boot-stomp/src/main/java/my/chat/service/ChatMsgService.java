package my.chat.service;

import my.chat.entity.ChatMsg;
import my.chat.mapper.ChatMsgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatMsgService {

    @Autowired
    private ChatMsgMapper chatMsgMapper;

    @Async
    public void insertChatMsg(ChatMsg chatMsg){
        chatMsgMapper.insertChatMsg(chatMsg);
    }

    public List<ChatMsg> lookTwoUserMsg(ChatMsg chatMsg){
        return chatMsgMapper.lookTwoUserMsg(chatMsg);
    }
}
