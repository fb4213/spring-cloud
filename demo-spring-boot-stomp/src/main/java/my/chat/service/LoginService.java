package my.chat.service;

import my.chat.common.pojo.bo.LoginBo;
import my.chat.common.pojo.vo.LoginVo;
import my.chat.mapper.LoginMapper;
import my.chat.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private LoginMapper loginMapper;

    public LoginVo login(LoginBo loginBo){
        String id = loginMapper.justLogin(loginBo);
        LoginVo loginVo = new LoginVo();
        loginVo.setUsername(id).setToken(JwtUtils.createToken(id));
        return loginVo;
    }

}
