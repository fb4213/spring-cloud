package my.chat.service;

import my.chat.entity.ChatFriends;
import my.chat.mapper.ChatFriendsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatFriendsService {

    @Autowired
    private ChatFriendsMapper chatFriendsMapper;

    public List<ChatFriends> lookUserAllFriends(String userId){
        return chatFriendsMapper.lookUserAllFriends(userId);
    }

}
