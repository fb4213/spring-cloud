package my.chat.mapper;

import my.chat.common.pojo.bo.LoginBo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LoginMapper {

    /** 判断登录 */
    String justLogin(LoginBo loginBo);

}
