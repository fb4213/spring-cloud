package my.chat.mapper;

import my.chat.entity.ChatFriends;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChatFriendsMapper {

    /** 查询所有的好友 */
    List<ChatFriends> lookUserAllFriends(String userId);

}