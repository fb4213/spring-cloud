package my.chat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class ChatMsg {

    private String id;
    private String toUserId;
    private String formUserId;
    private Date createTime;
    private String signFlag;
    private String msg;

}