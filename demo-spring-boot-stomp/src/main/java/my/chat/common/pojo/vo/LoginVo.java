package my.chat.common.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginVo {

    private Integer id;
    private Integer rid;
    private String username;
    private String token;
}
