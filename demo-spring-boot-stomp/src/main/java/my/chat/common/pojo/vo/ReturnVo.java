package my.chat.common.pojo.vo;

import lombok.Data;
import my.chat.common.enums.VoEnum;

import static my.chat.common.pojo.vo.Meta.SUCCESS;


@Data
public class ReturnVo<T> {

    private Meta meta;
    private T data;

    private ReturnVo(Meta meta, T data) {
        this.meta = meta;
        this.data = data;
    }

    public static ReturnVo success() {
        return success(null);
    }

    public static <T> ReturnVo<T> success(T data) {
        SUCCESS.setMsg("请求成功");
        return new ReturnVo<>(SUCCESS, data);
    }

    public static <T> ReturnVo<T> success(String msg, T data) {
        SUCCESS.setMsg(msg);
        return new ReturnVo<>(SUCCESS, data);
    }

    public static <T> ReturnVo<T> warning(VoEnum meta) {
        return new ReturnVo<>(new Meta(meta), null);
    }

    public static ReturnVo warning(int status, String msg) {
        return new ReturnVo(new Meta(status, msg), null);
    }

    public static <T> ReturnVo<T> warning(Meta meta, T data) {
        return new ReturnVo<>(meta, data);
    }
}
