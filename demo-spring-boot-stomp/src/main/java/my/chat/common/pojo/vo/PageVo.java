package my.chat.common.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class PageVo<T> {

    private long total;
    private int pagenum;

    private List<T> list;

    public PageVo(int pagenum, long total) {
        this.pagenum = pagenum;
        this.total = total;
    }
}
