package my.chat.common.pojo.bo;

import lombok.Data;

@Data
public class LoginBo {

    private String username;
    private String password;
}
