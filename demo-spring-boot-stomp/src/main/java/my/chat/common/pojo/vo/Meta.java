package my.chat.common.pojo.vo;

import my.chat.common.enums.VoEnum;

public class Meta {

    public static final Meta SUCCESS = new Meta(VoEnum.SUCCESS);

    private final int status;
    private String msg;

    public Meta(String msg) {
        this(200, msg);
    }

    public Meta(VoEnum voEnum) {
        this(voEnum.getStatus(), voEnum.getMsg());
    }

    public Meta(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
