package my.chat.common.pojo.bo;

import lombok.Data;

import javax.validation.constraints.Min;

/**
 * 分页查询参数
 * @author fengbo
 */
@Data
public class PageBo {

    @Min(0)
    private int pagenum = 0;

    @Min(0)
    private int pagesize = 0;

    public PageBo() {}

}
