package my.chat.common.execption;

public class WebBaseException extends RuntimeException {

    private final int status;
    private final String msg;

    public WebBaseException(int status, String msg) {
        super(msg);
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
