package my.chat.common.execption;

import lombok.extern.slf4j.Slf4j;
import my.chat.common.enums.VoEnum;
import my.chat.common.pojo.vo.ReturnVo;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ReturnVo methodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException e) {
        log.warn(" ====== Request url is [{}] ====== ", request.getRequestURL(), e);
        return ReturnVo.warning(VoEnum.PARAM_ERROR);
    }

    @ExceptionHandler(ValidationException.class)
    public ReturnVo validationException(HttpServletRequest request, ValidationException e) {
        log.warn(" ====== Request url is [{}] ====== ", request.getRequestURL(), e);
        return ReturnVo.warning(VoEnum.PARAM_ERROR);
    }

    @ExceptionHandler(WebBaseException.class)
    public ReturnVo webBaseException(HttpServletRequest request, WebBaseException e) {
        log.warn(" ====== Request url is [{}] ====== ", request.getRequestURL(), e);
        return ReturnVo.warning(e.getStatus(), e.getMsg());
    }

    @ExceptionHandler(Exception.class)
    public ReturnVo defaultErrorView(HttpServletRequest request, Exception e) {
        log.error(" ====== Request url is [{}] ====== ", request.getRequestURL(), e);
        return ReturnVo.warning(VoEnum.SYSTEM_ERROR);
    }
}
