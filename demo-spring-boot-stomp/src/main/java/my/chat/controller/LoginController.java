package my.chat.controller;

import my.chat.common.pojo.bo.LoginBo;
import my.chat.common.pojo.vo.LoginVo;
import my.chat.common.pojo.vo.ReturnVo;
import my.chat.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    @ResponseBody
    public ReturnVo<LoginVo> login(@RequestBody LoginBo loginBo) {
        return ReturnVo.success(loginService.login(loginBo));
    }
}
