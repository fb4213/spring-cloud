package my.nacos.consumer.controller;

import my.nacos.consumer.entity.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/list")
    public List<Goods> getListByTypeId(Long goodsTypeId) {
        return restTemplate.getForObject("http://provider/goods/list?goodsTypeId=" + goodsTypeId, List.class);
    }

}

