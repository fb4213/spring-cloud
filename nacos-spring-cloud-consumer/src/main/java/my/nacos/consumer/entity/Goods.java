package my.nacos.consumer.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 商品
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Data
public class Goods implements Serializable {

    private static final long serialVersionUID=1L;

    private Long id;

    private String goodsName;

    private BigDecimal goodsPrice;

    private String mainPic;

    private String goodsDesc;

    private Long cafesId;

    private Long typeId;

    private Date updateTime;

    private Date createTime;

}
