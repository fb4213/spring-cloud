package my.mall.controller;

import my.mall.common.pojo.LoginUser;
import my.mall.common.pojo.vo.MenusVo;
import my.mall.common.pojo.vo.ReturnVo;
import my.mall.config.auth.Login;
import my.mall.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HomeController {

    @Autowired
    private PermissionService permissionService;

    @GetMapping("/menus")
    public ReturnVo<List<MenusVo>> getMenusList(@Login LoginUser loginUser) {
        return ReturnVo.success(permissionService.getMenus(loginUser));
    }
}
