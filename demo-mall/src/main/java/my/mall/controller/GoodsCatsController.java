package my.mall.controller;


import my.mall.common.pojo.bo.PageBo;
import my.mall.common.pojo.vo.GoodsCatsVo;
import my.mall.common.pojo.vo.ReturnVo;
import my.mall.service.GoodsCatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 商品 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@RestController
public class GoodsCatsController {

    @Autowired
    private GoodsCatsService goodsCatsService;

    @GetMapping("/categories")
    public ReturnVo<List<GoodsCatsVo>> getCats(int type, @Validated PageBo pageBo) {
        return ReturnVo.success(goodsCatsService.getGoodsCats(type, pageBo));
    }
}

