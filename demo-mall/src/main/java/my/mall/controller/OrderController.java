package my.mall.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@RestController
@RequestMapping("/order")
public class OrderController {

}

