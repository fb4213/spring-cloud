package my.mall.controller;

import my.mall.common.pojo.bo.LoginBo;
import my.mall.common.pojo.bo.RegisterBo;
import my.mall.common.pojo.vo.LoginVo;
import my.mall.common.pojo.vo.ReturnVo;
import my.mall.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ReturnVo<LoginVo> login(@RequestBody @Valid LoginBo loginBo) {
        return userService.login(loginBo);
    }

    @PostMapping("/register")
    public ReturnVo<LoginVo> register(@RequestBody @Valid RegisterBo registerBo) {
        return userService.register(registerBo);
    }
}
