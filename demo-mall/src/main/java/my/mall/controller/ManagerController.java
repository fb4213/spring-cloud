package my.mall.controller;

import my.mall.common.pojo.LoginUser;
import my.mall.common.pojo.bo.ManagerBo;
import my.mall.common.pojo.bo.PageBo;
import my.mall.common.pojo.vo.ManagerVo;
import my.mall.common.pojo.vo.PageVo;
import my.mall.common.pojo.vo.ReturnVo;
import my.mall.config.auth.Login;
import my.mall.entity.Manager;
import my.mall.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/users")
@Validated
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @GetMapping
    public ReturnVo<PageVo<ManagerVo>> getManagerList(@Login LoginUser loginUser,
                                                      @RequestParam(value = "query", defaultValue = "") String query,
                                                      @Validated PageBo pageBo) {
        return ReturnVo.success(managerService.getManagerList(query, pageBo));
    }

    @PostMapping
    public ReturnVo<Manager> addManager(@RequestBody ManagerBo manager) {
        return ReturnVo.success(managerService.addManager(manager));
    }

    @PutMapping("/{id}")
    public ReturnVo<Manager> updateManager(@RequestBody ManagerBo managerBo) {
        return ReturnVo.success(managerService.updateManager(managerBo));
    }

    @GetMapping("/{id}")
    public ReturnVo<ManagerVo> getOne(@PathVariable("id") Integer id) {
        return ReturnVo.success(managerService.getManagerById(id));
    }

    @PutMapping("/{id}/state/{state}")
    public ReturnVo updateState(@PathVariable("id") @NotNull Integer id,
                                @PathVariable("state") @NotNull Boolean state) {
        managerService.markDelById(id, state);
        return ReturnVo.success();
    }

    @DeleteMapping("/{id}")
    public ReturnVo deleteManager(@PathVariable @NotNull Integer id) {
        managerService.deleteById(id);
        return ReturnVo.success();
    }
}
