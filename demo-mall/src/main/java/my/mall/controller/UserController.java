package my.mall.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2021-01-14
 */
@RestController
@RequestMapping("/user")
public class UserController {

}

