package my.mall.controller;

import my.mall.common.pojo.vo.ReturnVo;
import my.mall.entity.Role;
import my.mall.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/roles")
    public ReturnVo<List<Role>> getAllRole() {
        return ReturnVo.success(roleService.getAllRole());
    }
}
