package my.mall.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

}

