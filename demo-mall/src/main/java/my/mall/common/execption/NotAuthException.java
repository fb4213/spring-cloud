package my.mall.common.execption;

import my.mall.common.enums.VoEnum;

public class NotAuthException extends WebBaseException {

    public NotAuthException(VoEnum vo) {
        super(vo.getStatus(), vo.getMsg());
    }

    public NotAuthException(int status, String msg) {
        super(status, msg);
    }
}
