package my.mall.common.enums;

public enum VoEnum {

    SUCCESS(200, "请求成功"),

    LOGIN_FAIL(401, "Login Failed"),
    REGISTER_FAIL_REPEAT_USERNAME(402, "Username Repeat"),
    PARAM_ERROR(400, "参数错误"),

    NOT_AUTH(410, "Not Auth"),

    SYSTEM_ERROR(500, "系统错误"),
    ;

    private final int status;
    private final String msg;

    VoEnum(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
