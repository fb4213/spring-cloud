package my.mall.common.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ManagerVo {

    private Integer id;
    private String username;
    private String mobile;
    private String email;
    private Integer rid;
    private Integer mgState;
}
