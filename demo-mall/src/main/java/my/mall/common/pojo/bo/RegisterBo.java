package my.mall.common.pojo.bo;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
public class RegisterBo {

    @NotBlank
    @Length(min = 2, max = 12)
    private String username;
    @NotBlank
    private String password;
}
