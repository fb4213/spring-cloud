package my.mall.common.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginUser {

    private Integer userId;
    private String username;
    private Integer roleId;
    private Integer userState;
}
