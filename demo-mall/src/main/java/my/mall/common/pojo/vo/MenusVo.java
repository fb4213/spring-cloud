package my.mall.common.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class MenusVo {

    private String authName;
    private Integer order;
    private String path;

    private List<MenusVo> children;
}
