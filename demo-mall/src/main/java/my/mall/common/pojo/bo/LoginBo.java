package my.mall.common.pojo.bo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class LoginBo {

    @NotBlank
    @Length(min = 2, max = 12)
    private String username;
    @NotBlank
    private String password;
}
