package my.mall.common.pojo.bo;

import lombok.Data;

@Data
public class ManagerBo {

    private Integer id;
    private String username;
    private String password;
    private String email;
    private String mobile;
}
