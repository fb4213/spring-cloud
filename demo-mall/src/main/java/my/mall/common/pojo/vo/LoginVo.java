package my.mall.common.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginVo {

    private Long id;
    private String username;
    private String token;
}
