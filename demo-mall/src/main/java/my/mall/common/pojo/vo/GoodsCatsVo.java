package my.mall.common.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class GoodsCatsVo {

    private Integer catId;
    private String catName;
    private Integer catPid;
    private Integer catLevel;

    private List<GoodsCatsVo> children;
}
