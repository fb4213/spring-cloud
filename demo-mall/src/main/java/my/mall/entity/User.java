package my.mall.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author fengbo
 * @since 2021-01-14
 */
@Data
@Accessors(chain = true)
@Entity
@Table(name = "t_user")
public class User implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(unique = true, length = 32, nullable = false)
    private String username;

    @Column(length = 64, nullable = false)
    private String password;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
}
