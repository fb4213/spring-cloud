package my.mall.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Order implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    private Integer orderId;

    /**
     * 下订单会员id
     */
    private Integer userId;

    /**
     * 订单编号
     */
    private String orderNumber;

    /**
     * 订单总金额
     */
    private BigDecimal orderPrice;

    /**
     * 支付方式  0未支付 1支付宝  2微信  3银行卡
     */
    private String orderPay;

    /**
     * 订单是否已经发货
     */
    private String isSend;

    /**
     * 支付宝交易流水号码
     */
    private String tradeNo;

    /**
     * 发票抬头 个人 公司
     */
    private String orderFapiaoTitle;

    /**
     * 公司名称
     */
    private String orderFapiaoCompany;

    /**
     * 发票内容
     */
    private String orderFapiaoContent;

    /**
     * consignee收货人地址
     */
    private String consigneeAddr;

    /**
     * 订单状态： 0未付款、1已付款
     */
    private String payStatus;

    /**
     * 记录生成时间
     */
    private Integer createTime;

    /**
     * 记录修改时间
     */
    private Integer updateTime;


}
