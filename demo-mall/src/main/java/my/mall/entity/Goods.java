package my.mall.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Goods implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer goodsId;

    private String goodsName;

    private BigDecimal goodsPrice;

    private Integer goodsNumber;

    private Integer goodsWeight;

    private Integer catId;

    private String goodsIntroduce;

    private String goodsBigLogo;

    private String goodsSmallLogo;

    private String isDel;

    private Integer addTime;

    private Integer updTime;

    private Integer deleteTime;

    private Integer catOneId;

    private Integer catTwoId;

    private Integer catThreeId;

    private Integer hotMumber;

    private Integer isPromote;

    private Integer goodsState;

}
