package my.mall.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GoodsCats implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 分类id
     */
    private Integer catId;

    /**
     * 父级id
     */
    private Integer parentId;

    /**
     * 分类名称
     */
    private String catName;

    /**
     * 是否显示
     */
    private Integer isShow;

    /**
     * 分类排序
     */
    private Integer catSort;

    /**
     * 数据标记
     */
    private Integer dataFlag;

    /**
     * 创建时间
     */
    private Integer createTime;


}
