package my.mall.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Permission {

    private Integer psId;
    private Integer psPid;
    private String psName;
    private String psA;
    private Integer psLevel;
}
