package my.mall.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 商品-相册关联表
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GoodsPics implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    private Integer picsId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 相册大图800*800
     */
    private String picsBig;

    /**
     * 相册中图350*350
     */
    private String picsMid;

    /**
     * 相册小图50*50
     */
    private String picsSma;


}
