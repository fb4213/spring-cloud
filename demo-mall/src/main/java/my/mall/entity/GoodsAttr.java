package my.mall.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品-属性关联表
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GoodsAttr implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 属性id
     */
    private Integer attrId;

    /**
     * 商品对应属性的值
     */
    private String attrValue;

    /**
     * 该属性需要额外增加的价钱
     */
    private BigDecimal addPrice;


}
