package my.mall.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Manager {

    private Integer mgId;
    private String mgName;
    private String mgPwd;
    private Integer roleId;
    private Integer mgState;
    private String mgMobile;
    private String mgEmail;
    private int mgTime;
}
