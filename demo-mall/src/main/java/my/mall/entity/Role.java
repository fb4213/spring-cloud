package my.mall.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Role {

    private Integer id;
    private String roleName;
    private String roleDesc;
    private String psIds;
}
