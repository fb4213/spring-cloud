package my.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author fengbo
 */
@SpringBootApplication(scanBasePackages = {"my.mall"})
@EnableCaching
public class Application {

    public static void main(String[] args) {
	    SpringApplication.run(Application.class, args);
    }

}
