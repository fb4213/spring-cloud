package my.mall.dao;

import my.mall.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select id, password from t_user where username = :name", nativeQuery = true)
    User getUserByUserName(@Param("name") String username);

    boolean existsByUsername(String username);
}
