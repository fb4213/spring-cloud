package my.mall.service.impl;

import my.mall.service.GoodsAttrService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品-属性关联表 服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@Service
public class GoodsAttrServiceImpl implements GoodsAttrService {

}
