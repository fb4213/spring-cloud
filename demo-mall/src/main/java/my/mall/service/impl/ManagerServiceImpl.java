package my.mall.service.impl;

import my.mall.common.enums.VoEnum;
import my.mall.common.pojo.bo.ManagerBo;
import my.mall.common.pojo.vo.Meta;
import my.mall.entity.Manager;
import my.mall.common.pojo.bo.PageBo;
import my.mall.common.pojo.vo.*;
import my.mall.service.ManagerService;
import my.mall.util.JwtUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ManagerServiceImpl implements ManagerService {

    @Override
    public ReturnVo<LoginVo> login(String username, String password) {
        Manager manager = getManagerByName(username);
        if (manager == null) {
            return ReturnVo.warning(VoEnum.LOGIN_FAIL);
        }
        String pwd = DigestUtils.sha256Hex(password);
        if (!pwd.equals(manager.getMgPwd())) {
            return ReturnVo.warning(VoEnum.LOGIN_FAIL);
        }
        LoginVo loginVo = new LoginVo();
//        loginVo.setId(manager.getMgId()).setRid(manager.getRoleId())
//                .setUsername(username).setToken(JwtUtils.createToken(username));
        return ReturnVo.success(loginVo);
    }

    @Override
    public Manager getManagerByName(String username) {
//        return managerMapper.getManagerByName(username);
        return null;
    }

    @Override
    public PageVo<ManagerVo> getManagerList(String query, PageBo pageBo) {
//        Page<ManagerVo> page = PageHelper.startPage(pageBo.getPagenum(), pageBo.getPagesize()).doSelectPage(
//                ()-> managerMapper.selectManager(query));
//        PageVo<ManagerVo> pageVo = new PageVo<>(page.getPageNum(), page.getTotal());
//        pageVo.setList(page.getResult());
//        return pageVo;
        return null;
    }

    @Override
    public Manager addManager(ManagerBo managerBo) {
//        String pwd = DigestUtils.sha256Hex(managerBo.getPassword());
//        Manager manager = new Manager();
//        manager.setMgName(managerBo.getUsername()).setMgPwd(pwd)
//                .setMgEmail(managerBo.getEmail()).setMgMobile(managerBo.getMobile())
//                .setMgTime((int) (System.currentTimeMillis() / 1000));
//        managerMapper.insertManager(manager);
//        return manager;

        return null;
    }

    @Override
    public Manager updateManager(ManagerBo managerBo) {
        Manager manager = new Manager();
//        manager.setMgMobile(managerBo.getMobile()).setMgEmail(managerBo.getEmail())
//                .setMgId(managerBo.getId());
//        managerMapper.updateManager(manager);
        return manager;
    }

    @Override
    public ManagerVo getManagerById(Integer id) {
        ManagerVo managerVo = new ManagerVo();
//        Manager manager = managerMapper.getManagerById(id);
//        managerVo.setRid(manager.getRoleId()).setUsername(manager.getMgName());
        return managerVo;
    }

    @Override
    public void markDelById(Integer id, Boolean state) {
//        managerMapper.markDelById(id, state);
    }

    @Override
    public void deleteById(Integer id) {
//        managerMapper.deleteById(id);
    }
}
