package my.mall.service.impl;

import my.mall.service.GoodsPicsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品-相册关联表 服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
@Service
public class GoodsPicsServiceImpl implements GoodsPicsService {

}
