package my.mall.service;

import my.mall.common.pojo.LoginUser;
import my.mall.common.pojo.vo.MenusVo;

import java.util.List;

public interface PermissionService {

    List<MenusVo> getMenus(LoginUser loginUser);
}
