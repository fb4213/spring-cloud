package my.mall.service;

import my.mall.common.pojo.bo.ManagerBo;
import my.mall.entity.Manager;
import my.mall.common.pojo.bo.PageBo;
import my.mall.common.pojo.vo.LoginVo;
import my.mall.common.pojo.vo.ManagerVo;
import my.mall.common.pojo.vo.PageVo;
import my.mall.common.pojo.vo.ReturnVo;

public interface ManagerService {

    ReturnVo<LoginVo> login(String username, String password);

    Manager getManagerByName(String username);

    PageVo<ManagerVo> getManagerList(String query, PageBo pageBo);

    Manager addManager(ManagerBo manager);

    Manager updateManager(ManagerBo managerBo);

    ManagerVo getManagerById(Integer id);

    void markDelById(Integer id, Boolean state);

    void deleteById(Integer id);
}
