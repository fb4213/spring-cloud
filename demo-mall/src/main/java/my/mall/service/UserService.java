package my.mall.service;

import my.mall.common.pojo.bo.LoginBo;
import my.mall.common.pojo.bo.RegisterBo;
import my.mall.common.pojo.vo.LoginVo;
import my.mall.common.pojo.vo.ReturnVo;
import my.mall.entity.User;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author fengbo
 * @since 2021-01-14
 */
public interface UserService {

    User getUserById(Long id);

    ReturnVo<LoginVo> login(LoginBo loginBo);

    ReturnVo<LoginVo> register(RegisterBo registerBo);
}
