package my.mall.service;

import my.mall.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> getAllRole();
}
