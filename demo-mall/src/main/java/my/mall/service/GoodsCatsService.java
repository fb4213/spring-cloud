package my.mall.service;

import my.mall.common.pojo.bo.PageBo;
import my.mall.common.pojo.vo.GoodsCatsVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengbo
 * @since 2021-01-22
 */
public interface GoodsCatsService {

    List<GoodsCatsVo> getGoodsCats(int type, PageBo pageBo);

}
