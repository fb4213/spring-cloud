package my.nacos.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Cookie和Token：https://www.jianshu.com/p/ce9802589143
 * 权限控制：https://www.jianshu.com/p/1e974fc91f74
 */
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"my.nacos.auth"})
public class NacosAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosAuthApplication.class, args);
    }
}