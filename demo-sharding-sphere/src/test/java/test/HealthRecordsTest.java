package test;

import my.shardingsphere.Application;
import my.shardingsphere.service.HealthRecordService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class HealthRecordsTest {

    @Autowired
    private HealthRecordService healthRecordService;

    @Test
    public void testInsert() throws SQLException {
        healthRecordService.processHealthRecords();
    }

}
