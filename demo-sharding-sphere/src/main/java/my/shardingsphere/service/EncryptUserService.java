package my.shardingsphere.service;

import my.shardingsphere.entity.EncryptUser;

import java.sql.SQLException;
import java.util.List;

public interface EncryptUserService {
	
	void processEncryptUsers() throws SQLException;
	
	List<EncryptUser> getEncryptUsers() throws SQLException;

}
