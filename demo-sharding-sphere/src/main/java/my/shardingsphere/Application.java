package my.shardingsphere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author fengbo
 */
@SpringBootApplication(scanBasePackages = {"my.shardingsphere"})
@EnableCaching
public class Application {

    public static void main(String[] args) {
	    SpringApplication.run(Application.class, args);
    }

}
