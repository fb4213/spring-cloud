package my.shardingsphere.repository;

import my.shardingsphere.entity.HealthRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HealthRecordRepository extends BaseRepository<HealthRecord, Long> {

}
