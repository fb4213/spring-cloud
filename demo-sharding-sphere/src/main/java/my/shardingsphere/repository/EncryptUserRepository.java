package my.shardingsphere.repository;

import my.shardingsphere.entity.EncryptUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EncryptUserRepository extends BaseRepository<EncryptUser, Long> {

}
