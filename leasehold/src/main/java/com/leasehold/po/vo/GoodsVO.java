package com.leasehold.po.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 参数校验：https://mp.weixin.qq.com/s/Vmbp5jEm_aKfo_O20gZqcg
 * @author fengbo
 * @since 2020-05-15
 */
@Data
@ApiModel(value="Goods对象", description="商品")
public class GoodsVO implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "商品名称")
    @NotNull
    @Length(min = 2, max = 5)
    private String goodsName;

    @ApiModelProperty(value = "商品价格")
    @NotNull
    private BigDecimal goodsPrice;

    @ApiModelProperty(value = "商品主图")
    private String mainPic;

    @ApiModelProperty(value = "商品描述")
    private String goodsDesc;
}
