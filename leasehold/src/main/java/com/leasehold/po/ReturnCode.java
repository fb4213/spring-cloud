package com.leasehold.po;

/**
 * @author fengbo
 */
public enum ReturnCode {
    /** 成功 */
    SUCCESS(0, "操作成功"),
    MISSING_TOKEN(6000,"Missing token."),
    PARAM_ERROR(8000,"请求参数有误"),

    PARAM_INCOMPLETE(1000, "参数有误"),
    SYS_FAILED(500, "系统异常"),

    END(1, "秒杀结束"),
    NOT_REGISTER(-2, "未注册"),
    REPEAT_KILL(-1, "重复秒杀"),


    WECHAT_PAY_ERROR(9001,"微信支付失败"),
    INSUFFICIENT_BALANCE(9100, "余额不足"),
    REPEAT_WITHDRAWAL(9101, "重复提单"),
    WITHDRAWAL_CODE_ERROR(9102, "订单编号异常"),
    REPEAT_AUDIT(9104, "重复审核"),
    WITHDRAWAL_TIME(9105, "平台结算中，请稍后再试"),
    WITHDRAWAL_LIMIT_OVER(9106, "提现单笔上限5000元，提现金额请输入1-5000元"),
    AUDIT_ERROR(9107, "审核失败"),
    APPLY_ERROR(9108, "提现失败"),
    WITHDRAWAL_MOUTH_NUMBER_OUT(9110, "提单超过每月5次限制"),
    WITHDRAWAL_DAY_NUMBER_OUT(9111, "提单超过每天3次限制"),
    ;

    /** 编码 */
    private int code;
    /** 消息 */
    private String msg;

    ReturnCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String message) {
        this.msg = msg;
    }
}
