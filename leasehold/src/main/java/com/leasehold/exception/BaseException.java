package com.leasehold.exception;

import com.leasehold.po.ReturnCode;
import lombok.Data;

@Data
public class BaseException extends RuntimeException {
    private int code;
    private String msg;

    public BaseException(ReturnCode returnCode) {
        super(returnCode.getMsg());
        setCode(returnCode.getCode());
        setMsg(returnCode.getMsg());
    }

    public BaseException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
