package com.leasehold.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.leasehold.entity.User;
import com.leasehold.po.vo.VoData;
import com.leasehold.service.UserService;
import com.leasehold.util.ReturnUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Api(tags = "用户管理 by fengbo")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("获取用户基础信息")
    @GetMapping("/msg")
    public VoData<User> getUserMsg() {
        // TODO 考虑如何获取
        String accountCode = "wxfdbf1ae6d43101dc";
        User user = userService.getOne(new LambdaQueryWrapper<User>().eq(User::getAccountCode, accountCode));
        return ReturnUtils.success(user);
    }

}

