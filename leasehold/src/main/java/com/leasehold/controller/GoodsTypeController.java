package com.leasehold.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.leasehold.entity.GoodsType;
import com.leasehold.po.vo.VoData;
import com.leasehold.service.GoodsTypeService;
import com.leasehold.util.ReturnUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Api(tags = "商品类型管理 by fengbo")
@RestController
@RequestMapping("/goods-type")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    @ApiOperation("获取商品类型列表接口 level-类型级别 pid-父ID")
    @GetMapping("/list")
    public VoData<List<GoodsType>> getGoodsType(Integer level, Integer pid) {
        List<GoodsType> list = goodsTypeService.list(new LambdaQueryWrapper<GoodsType>()
                .eq(level != null && level > 0, GoodsType::getTypeLevel, level)
                .eq(pid != null && pid > 0, GoodsType::getParentId, pid)
                .eq(GoodsType::getIsDel, Boolean.FALSE)
        );
        return ReturnUtils.success(list);
    }
}

