package com.leasehold.controller;

import com.leasehold.entity.Cafes;
import com.leasehold.po.vo.VoData;
import com.leasehold.service.CafesService;
import com.leasehold.util.ReturnUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Api(tags = "店铺管理 by fengbo")
@RestController
@RequestMapping("/cafes")
@Validated
public class CafesController {

    @Autowired
    private CafesService cafesService;

    @ApiOperation("根据ID获取网吧基础信息")
    @GetMapping("/{id}")
    public VoData<Cafes> getOne(@PathVariable("id") @Max(100L) Long id) {
        Cafes cafes = cafesService.getById(id);
        return ReturnUtils.success(cafes);
    }
}

