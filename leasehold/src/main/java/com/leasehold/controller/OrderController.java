package com.leasehold.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.leasehold.entity.Order;
import com.leasehold.po.bo.PageBO;
import com.leasehold.po.vo.VoData;
import com.leasehold.service.OrderService;
import com.leasehold.util.ReturnUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 租赁日志表 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Api(tags = "订单管理 by fengbo")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @ApiOperation("根据状态分页获取订单信息 status-订单状态(1-使用中,2-代付款,3-已完成)")
    @GetMapping("/list")
    public VoData<IPage<Order>> getListByUser(Integer status, PageBO page) {
        // TODO 考虑如何获取
        String accountCode = "";
        IPage<Order> result = new Page<>(page.getCurrent(), page.getSize());
        result = orderService.page(result, new LambdaQueryWrapper<Order>()
                .eq(status != null && status > 0, Order::getLeaseStatus, status)
                .eq(Order::getAccountCode, accountCode)
                .orderByDesc(Order::getCreateTime));
        return ReturnUtils.success(result);
    }

}

