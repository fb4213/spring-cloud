package com.leasehold.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.leasehold.entity.Goods;
import com.leasehold.po.bo.PageBO;
import com.leasehold.po.vo.GoodsVO;
import com.leasehold.po.vo.VoData;
import com.leasehold.service.GoodsService;
import com.leasehold.util.ReturnUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Api(tags = "商品管理 by fengbo")
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @ApiOperation("根据商品类型ID分页获取商品列表 goodsTypeId-商品类型ID")
    @GetMapping("/list")
    public VoData<IPage<Goods>> getListByTypeId(Long goodsTypeId, PageBO page) {
        // TODO 需要考虑获取方式
        Long cafesId = 1L;
        IPage<Goods> result = new Page<>(page.getCurrent(), page.getSize());
        result = goodsService.page(result, new LambdaQueryWrapper<Goods>()
                .eq(Goods::getTypeId, goodsTypeId).eq(Goods::getCafesId, cafesId)
        );
        return ReturnUtils.success(result);
    }

    @PostMapping("/save")
    public VoData saveGoods(@RequestBody @Validated GoodsVO goodsVO) {

        return ReturnUtils.success();
    }
}

