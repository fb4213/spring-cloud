package com.leasehold.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("lease_goods_type")
@ApiModel(value="GoodsType对象", description="")
public class GoodsType implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "商品类型")
    private String typeName;

    @ApiModelProperty(value = "商品类型图")
    private String typeImg;

    @ApiModelProperty(value = "类型级别")
    private Integer typeLevel;

    @ApiModelProperty(value = "父类型id")
    private Long parentId;

    @ApiModelProperty(value = "是否删除1-true 0-false")
    private Boolean isDel;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}
