package com.leasehold.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 租赁日志表
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("lease_order")
@ApiModel(value="Order对象", description="租赁日志表")
public class Order implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "租赁开始时间")
    private Date startTime;

    @ApiModelProperty(value = "租赁截止时间")
    private Date endTime;

    @ApiModelProperty(value = "租赁用户")
    private String accountCode;

    @ApiModelProperty(value = "租赁状态")
    private Integer leaseStatus;

    @ApiModelProperty(value = "本次租赁金额")
    private BigDecimal leaseAmount;

    @ApiModelProperty(value = "商品id")
    private Long goodsId;

    @ApiModelProperty(value = "商品价格")
    private BigDecimal goodsPrice;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品主图")
    private String mainPic;

    @ApiModelProperty(value = "商品主图")
    private Long cafesId;

    @ApiModelProperty(value = "商品主图")
    private Integer tableId;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}
