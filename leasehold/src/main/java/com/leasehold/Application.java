package com.leasehold;

import my.springboot.starter.EnableDemoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author fengbo
 */
@SpringBootApplication(scanBasePackages = {"com.leasehold"})
@EnableDemoConfiguration
@EnableCaching
public class Application {

    public static void main(String[] args) {
	    SpringApplication.run(Application.class, args);
    }

}
