package com.leasehold.mapper;

import com.leasehold.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
