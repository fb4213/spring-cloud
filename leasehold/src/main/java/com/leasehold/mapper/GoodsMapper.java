package com.leasehold.mapper;

import com.leasehold.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
