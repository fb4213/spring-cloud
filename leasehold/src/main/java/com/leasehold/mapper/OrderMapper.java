package com.leasehold.mapper;

import com.leasehold.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 租赁日志表 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface OrderMapper extends BaseMapper<Order> {

}
