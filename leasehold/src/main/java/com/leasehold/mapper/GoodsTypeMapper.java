package com.leasehold.mapper;

import com.leasehold.entity.GoodsType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface GoodsTypeMapper extends BaseMapper<GoodsType> {

}
