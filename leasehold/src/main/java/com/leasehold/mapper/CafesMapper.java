package com.leasehold.mapper;

import com.leasehold.entity.Cafes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网吧 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface CafesMapper extends BaseMapper<Cafes> {

}
