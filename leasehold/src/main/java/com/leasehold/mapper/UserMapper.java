package com.leasehold.mapper;

import com.leasehold.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface UserMapper extends BaseMapper<User> {

    List<User> queryByIds();

}
