package com.leasehold.service;

import com.leasehold.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface PayLogService extends IService<PayLog> {

}
