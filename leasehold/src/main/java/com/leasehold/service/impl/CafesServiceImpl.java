package com.leasehold.service.impl;

import com.leasehold.entity.Cafes;
import com.leasehold.mapper.CafesMapper;
import com.leasehold.service.CafesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 网吧 服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Service
public class CafesServiceImpl extends ServiceImpl<CafesMapper, Cafes> implements CafesService {

}
