package com.leasehold.service.impl;

import com.leasehold.entity.Goods;
import com.leasehold.mapper.GoodsMapper;
import com.leasehold.service.GoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {

}
