package com.leasehold.service.impl;

import com.leasehold.entity.PayLog;
import com.leasehold.mapper.PayLogMapper;
import com.leasehold.service.PayLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付日志表 服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Service
public class PayLogServiceImpl extends ServiceImpl<PayLogMapper, PayLog> implements PayLogService {

}
