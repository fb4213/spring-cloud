package com.leasehold.service.impl;

import com.leasehold.entity.GoodsType;
import com.leasehold.mapper.GoodsTypeMapper;
import com.leasehold.service.GoodsTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Service
public class GoodsTypeServiceImpl extends ServiceImpl<GoodsTypeMapper, GoodsType> implements GoodsTypeService {

}
