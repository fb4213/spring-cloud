package com.leasehold.service.impl;

import com.leasehold.entity.Order;
import com.leasehold.mapper.OrderMapper;
import com.leasehold.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 租赁日志表 服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

}
