package com.leasehold.service;

import com.leasehold.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface GoodsService extends IService<Goods> {

}
