package com.leasehold.service;

import com.leasehold.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 租赁日志表 服务类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface OrderService extends IService<Order> {

}
