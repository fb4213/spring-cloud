package com.leasehold.service;

import com.leasehold.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface UserService extends IService<User> {

}
