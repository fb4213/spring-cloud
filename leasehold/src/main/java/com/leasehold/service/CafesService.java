package com.leasehold.service;

import com.leasehold.entity.Cafes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 网吧 服务类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface CafesService extends IService<Cafes> {

}
