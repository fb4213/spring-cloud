package com.leasehold.common;

public interface Constant {

    String PAY_USER_KEY = "PAY:USER:%s";
    String PAY_ORDER_KEY = "PAY:ORDER:%s";
    String MSG_RECOMMEND_KEY = "MSG:RECOMMEND:%s";

    /** 日期常量 */
    String NORMAL_DATE_FORMAT = "yyyy-MM-dd";
    String NORMAL_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /** 支付是否审核限制常量（单位分） */
    int PAY_AUDIT_LIMIT = 50000;
    /** 字段缓存key */
    String DICT_KEY = "DICT:KEY:%s";

    String CODE2SESSION_URL = "https://api.weixin.qq.com/sns/jscode2session";
    String WECHAT_PUSH_MESSAGE_ACCESS_TOKEN = "WECHAT_PUSH_MESSAGE_ACCESS_TOKEN";
}
