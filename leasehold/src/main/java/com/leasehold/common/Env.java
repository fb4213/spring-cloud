package com.leasehold.common;

/**
 * @author fengbo
 * @date 2018/11/15
 */
public interface Env {
    /** 开发环境 */
    String DEV = "dev";
    /** 测试环境 */
    String TEST = "test";
    /** 线上环境 */
    String PROD = "prod";
}
