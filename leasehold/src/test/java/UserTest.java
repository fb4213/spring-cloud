import com.leasehold.Application;
import com.leasehold.entity.User;
import com.leasehold.mapper.UserMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testUserMapper() {
        // 如果查询出来的字段都是Null，则Mybatis实际映射出来的实体类为Null
        List<User> users = userMapper.queryByIds();
        Assert.assertNotNull(users);
    }
}
