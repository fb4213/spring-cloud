package my.chat.service;

import my.chat.entity.Login;
import my.chat.mapper.LoginMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private LoginMapper loginMapper;

    public String justLogin(Login login){
        return loginMapper.justLogin(login);
    }

}
