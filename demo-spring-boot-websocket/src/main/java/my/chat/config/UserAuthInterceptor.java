package my.chat.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UserAuthInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String user=(String) request.getSession().getAttribute("userid");
        if(user!=null){
            return true;
        }
        response.sendRedirect(request.getContextPath() + "/");
        return false;
    }
}
