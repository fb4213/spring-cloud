package my.chat.mapper;

import my.chat.entity.ChatFriends;

import java.util.List;

public interface ChatFriendsMapper {

    /** 查询所有的好友 */
    List<ChatFriends> lookUserAllFriends(String userId);

}