package my.chat.mapper;

import my.chat.entity.Login;

public interface LoginMapper {

    /** 判断登录 */
    String justLogin(Login login);

}
