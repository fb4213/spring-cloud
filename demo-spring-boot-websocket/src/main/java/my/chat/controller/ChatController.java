package my.chat.controller;

import com.alibaba.fastjson.JSONObject;
import my.chat.entity.ChatFriends;
import my.chat.entity.ChatMsg;
import my.chat.service.ChatFriendsService;
import my.chat.service.ChatMsgService;
import my.chat.service.LoginService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Controller
public class ChatController {

    @Autowired
    private ChatFriendsService chatFriendsService;
    @Autowired
    private ChatMsgService chatMsgService;
    @Autowired
    private LoginService loginService;

    /**
     * 跳转到聊天
     */
    @GetMapping("/chat/view")
    public String toChat(){
        return "/chat/chats";
    }

    /***
     * 查询用户的好友
     */
    @PostMapping("/chat/lkfriends")
    @ResponseBody
    public List<ChatFriends> lkFriends(HttpSession session) {
        String userId = (String) session.getAttribute("userId");
        return chatFriendsService.lookUserAllFriends(userId);
    }

    /***
     * 查询两个用户之间的聊天记录
     */
    @PostMapping("/chat/lkuschatmsg/{reviceuserid}")
    @ResponseBody
    public List<ChatMsg> lkFriends(HttpSession session, @PathVariable("reviceuserid") String reviceuserid) {
        String userId = (String) session.getAttribute("userId");
        return chatMsgService.lookTwoUserMsg(new ChatMsg().setSendUserId(userId).setAcceptUserId(reviceuserid));
    }

    /**
     * 上传聊天图片
     */
    @PostMapping(value = "/chat/upimg")
    @ResponseBody
    public JSONObject upauz(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
        JSONObject res = new JSONObject();
        JSONObject resUrl = new JSONObject();
        LocalDate today = LocalDate.now();
        Instant timestamp = Instant.now();
        String ext = FilenameUtils.getExtension(file.getOriginalFilename());
        String filenames = today + String.valueOf(timestamp.toEpochMilli()) + "."+ext;
        file.transferTo(new File("g:\\" + filenames));
        resUrl.put("src", "/pic/" + filenames);
        res.put("msg", "");
        res.put("code", 0);
        res.put("data", resUrl);
        return res;
    }

    /***
     * Ajax上传web界面js录制的音频数据
     */
    @PostMapping("/chat/audio")
    @ResponseBody
    public JSONObject upaudio(@RequestParam(value = "file") MultipartFile file) throws IOException {
        JSONObject res = new JSONObject();
        JSONObject resUrl = new JSONObject();
        LocalDate today = LocalDate.now();
        Instant timestamp = Instant.now();
        String filenames = today  + String.valueOf(timestamp.toEpochMilli()) + ".mp3";
        String pathname = "D:\\chat\\" + filenames;
        file.transferTo(new File(pathname));
        resUrl.put("src", "/pic/"+filenames);
        res.put("msg", "");
        res.put("data", resUrl);
        return res;
    }
}
