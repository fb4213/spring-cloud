package my.chat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class ChatFriends {

    private String friendUserId;
    private String friendUsername;
    private String friendNickname;
    private String friendFaceImage;
}