package my.nacos.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import my.nacos.provider.entity.Goods;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
