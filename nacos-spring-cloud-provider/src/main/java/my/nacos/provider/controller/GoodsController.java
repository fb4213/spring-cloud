package my.nacos.provider.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import my.nacos.provider.entity.Goods;
import my.nacos.provider.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Api(tags = "Test by fengbo")
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @ApiOperation("List接口 goodsTypeId-类型ID")
    @GetMapping("/list")
    public List<Goods> getListByTypeId(Long goodsTypeId) {
        Long cafesId = 1L;
        return goodsService.list(new LambdaQueryWrapper<Goods>()
                .eq(Goods::getTypeId, goodsTypeId).eq(Goods::getCafesId, cafesId)
        );
    }

}

