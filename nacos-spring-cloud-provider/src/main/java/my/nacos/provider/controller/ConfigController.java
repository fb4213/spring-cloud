package my.nacos.provider.controller;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("config")
public class ConfigController implements EnvironmentAware {

    @NacosValue(value = "${useLocalCache:false}", autoRefreshed = true)
    private boolean useLocalCache;

    private Environment env;

    @GetMapping("/get")
    public boolean get() {
        return useLocalCache;
    }

    @GetMapping("/get/{key}")
    public String get(@PathVariable String key) {
        return env.getProperty(key);
    }

    @NacosInjected
    private NamingService namingService;

    @GetMapping("/discovery")
    public List<Instance> getInstances(@RequestParam String serviceName) throws NacosException {
        return namingService.getAllInstances(serviceName);
    }

    @Override
    public void setEnvironment(Environment env) {
        this.env = env;
    }
}