package my.nacos.provider.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import my.nacos.provider.entity.Goods;
import my.nacos.provider.mapper.GoodsMapper;
import my.nacos.provider.service.GoodsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {

}
