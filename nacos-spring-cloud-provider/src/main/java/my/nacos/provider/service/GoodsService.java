package my.nacos.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import my.nacos.provider.entity.Goods;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
public interface GoodsService extends IService<Goods> {

}
