[SpringBoot基础教程](http://blog.didispace.com/Spring-Boot%E5%9F%BA%E7%A1%80%E6%95%99%E7%A8%8B/) | [SpringBoot热部署](https://segmentfault.com/a/1190000009488038) | [SpringCloud基础教程](http://blog.didispace.com/spring-cloud-learning/) | [SpringCloud教程](https://github.com/forezp/SpringCloudLearning)

### 技能卡
<table align="center">
    <tr><td rowspan="4">Web基础</td>
                 <td>Spring</td><td>熟悉源码</td><td>*</td></tr>
    <tr><td>SpringBoot</td><td>熟练使用</td><td>*</td></tr>
    <tr><td>Mybatis</td><td>熟练使用</td><td>*</td></tr>
    <tr><td>Mybatis-Plus</td><td>熟练使用</td><td>*</td></tr>
    <tr><td rowspan="3">数据存储</td>
                 <td>RocketMQ</td><td>了解原理+熟练使用</td><td>*</td></tr>
    <tr><td>Redis</td><td>了解原理+熟练使用</td><td>*</td></tr>
    <tr><td>MySQL</td><td>了解原理+熟练使用</td><td>*</td></tr>
    <tr><td rowspan="3">网络编程</td>
                 <td>Netty</td><td>精通源码</td><td></td></tr>
    <tr><td>HTTP服务器</td><td>熟悉源码</td><td>*</td></tr>
    <tr><td>HTTP客户端</td><td>熟悉源码+熟练使用</td><td>*</td></tr>
    <tr><td rowspan="6">分布式</td>
                 <td colspan="3">SpringCloud</td></tr>
    <tr><td>Nacos</td><td>熟练使用</td><td>*</td></tr>
    <tr><td>OpenFeign</td><td>熟练使用</td><td>*</td></tr>
    <tr><td>Gateway</td><td>熟练使用</td><td>*</td></tr>
    <tr><td>XXL-Job</td><td>了解原理+熟练使用</td><td>*</td></tr>
    <tr><td>Dubbo(RPC)</td><td>了解原理</td><td>*</td></tr>
    <tr><td rowspan="4">工具类</td>
                 <td>JSON解析工具</td><td>会用即可</td><td>*</td></tr>
    <tr><td>HuTool等</td><td>会用即可</td><td>*</td></tr>
    <tr><td>Python工具</td><td>会用即可</td><td>*</td></tr>
    <tr><td colspan="3">Flask   爬虫  Raspberry   数据分析</td></tr>
</table>
  