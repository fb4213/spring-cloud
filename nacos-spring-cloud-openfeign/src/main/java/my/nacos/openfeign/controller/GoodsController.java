package my.nacos.openfeign.controller;

import my.nacos.openfeign.entity.Goods;
import my.nacos.openfeign.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @GetMapping("/list")
    public List<Goods> getListByTypeId(Long goodsTypeId) {
        return goodsService.listGoods(goodsTypeId);
    }
}
