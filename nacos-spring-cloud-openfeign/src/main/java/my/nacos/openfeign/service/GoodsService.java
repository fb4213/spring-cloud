package my.nacos.openfeign.service;

import my.nacos.openfeign.entity.Goods;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 商品服务类
 * </p>
 *
 * @author fengbo
 * @since 2020-05-15
 */
@FeignClient("provider")
public interface GoodsService {

    @GetMapping("/goods/list")
    List<Goods> listGoods(@RequestParam("goodsTypeId") Long goodsTypeId);
}
